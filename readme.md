API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Contents
--------
* [Installation](docs/installation.md)
    - [Requirements](docs/installation.md#markdown-header-requirements)
    - [Lumen](docs/installation.md#markdown-header-lumen)
    - [Laravel](docs/installation.md#markdown-header-laravel)
* [Implementation](docs/implementation.md)
    - [Package Classes](docs/implementation.md#markdown-header-package-classes)
    - [Models](docs/implementation.md#markdown-header-models)
    - [Repositories](docs/implementation.md#markdown-header-repositories)
    - [Controllers](docs/implementation.md#markdown-header-controllers)
* [Files Convention](docs/files_convention.md)
    - [Naming Convention](docs/files_convention.md#markdown-header-naming-convention)
    - [Namespaces](docs/files_convention.md#markdown-header-namespaces)
* [IDs Strategies and Migrations](docs/ids_strategies_migrations.md)
    - [Unpredictable IDs Strategy](docs/ids_strategies_migrations.md#markdown-header-unpredictable-ids-strategy)
    - [AutoIncrement IDs Strategy](docs/ids_strategies_migrations.md#markdown-header-autoincrement-ids-strategy)
    - [String IDs Migrations](docs/ids_strategies_migrations.md#markdown-header-string-ids-migrations)
    - [Increments IDs Migrations](docs/ids_strategies_migrations.md#markdown-header-increments-ids-migrations)
* [Routing](docs/routing.md)
* [Exceptions and Handlers](docs/exceptions_handlers.md)
    - [Exceptions](docs/exceptions_handlers.md#markdown-header-exceptions)
    - [Handlers](docs/exceptions_handlers.md#markdown-header-handlers)
* [Transformers](docs/transformers.md)
    - [Includes](docs/transformers.md#markdown-header-includes)
    - [Include Collection](docs/transformers.md#markdown-header-include-collection)
    - [Include Resource](docs/transformers.md#markdown-header-include-resource)
* [Hooks Functions](docs/hooks_functions.md)
    - [Saving Actions](docs/hooks_functions.md#markdown-header-saving-actions)
    - [Deleting Actions](docs/hooks_functions.md#markdown-header-deleting-actions)
* [Relationships](docs/relationships.md)
    - [ManyToMany](docs/relationships.md#markdown-header-manytomany)
    - [ManyToMany with extra attributes](docs/relationships.md#markdown-header-manytomanywithextraattributes)
* [Unit Tests](docs/unit_tests.md)
    - [Create Application](docs/unit_tests.md#markdown-header-create-application)
    - [Facades](docs/unit_tests.md#markdown-header-facades)
    - [Database](docs/unit_tests.md#markdown-header-database)
* [Example](docs/example.md)
    - [Models](docs/example.md#markdown-header-models)
    - [Repositories](docs/example.md#markdown-header-repositories)
    - [Controllers](docs/example.md#markdown-header-controllers)
    - [Transformers](docs/example.md#markdown-header-transformers)
    - [Routing](docs/example.md#markdown-header-routing)
