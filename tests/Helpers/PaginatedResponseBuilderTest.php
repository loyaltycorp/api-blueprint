<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Helpers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as PaginatorContract;
use LoyaltyCorp\ApiBlueprint\Helpers\PaginatedResponseBuilder;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface;
use LoyaltyCorp\ApiBlueprint\Transformers\ResourceTransformer;
use Mockery;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\HelpersTestCase;

class PaginatedResponseBuilderTest extends HelpersTestCase
{
    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function testGetters()
    {
        $paginator = Mockery::mock(LengthAwarePaginator::class);

        $builder = new PaginatedResponseBuilder(
            $paginator,
            new ResourceTransformer(),
            'resourceKey'
        );

        self::assertInstanceOf(PaginatorContract::class, $builder->getPaginator());
        self::assertInstanceOf(ResourceTransformerInterface::class, $builder->getTransformer());
        self::assertTrue(\is_string($builder->getResourceKey()));
    }
}
