<?php declare(strict_types=1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Helpers;

use LoyaltyCorp\ApiBlueprint\Helpers\Interfaces\QueryModifierInterface;
use LoyaltyCorp\ApiBlueprint\Helpers\QueryModifier;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\HelpersTestCase;

class QueryModifierTest extends HelpersTestCase
{
    public function testApplyFilters()
    {
        $filters = [
            'fillable1' => '[>,1]',
            'fillable2' => '[1]',
            'fillable3' => '1,2,3',
            'fillable4' => '1',
            'fillable_fake' => '1'
        ];

        $resource = $this->mockResourceForFilters();
        $modifier = $this->getQueryModifier();

        self::assertInstanceOf(QueryModifierInterface::class, $modifier->applyFilters($resource, $filters));
    }

    public function testApplyOrderBy()
    {
        $resource = $this->mockResourceForOrderBy();
        $modifier = $this->getQueryModifier();

        self::assertInstanceOf(QueryModifierInterface::class, $modifier->applyOrderBy($resource, 'fillable1,desc'));
    }

    public function testApplyOrderByWithInvalidField()
    {
        $resource = $this->mockResourceForOrderByWithInvalidField();
        $modifier = $this->getQueryModifier();

        self::assertInstanceOf(QueryModifierInterface::class, $modifier->applyOrderBy($resource, 'invalid,desc'));
    }

    public function testApplySearch()
    {
        $resource = $this->mockResourceForSearch();
        $modifier = $this->getQueryModifier();

        self::assertInstanceOf(QueryModifierInterface::class, $modifier->applySearch($resource, 'search'));
    }

    public function testApplySearchWithNull()
    {
        $resource = $this->mockResourceForSearchWithNull();
        $modifier = $this->getQueryModifier();

        self::assertInstanceOf(QueryModifierInterface::class, $modifier->applySearch($resource));
    }

    public function testGetSearchClosure()
    {
        $modifier = $this->getQueryModifier();
        $getSearchClosure = $this->getMethodAsPublic(QueryModifier::class, 'getSearchClosure');
        $closure = $getSearchClosure->invokeArgs($modifier, ['search']);

        $closure($this->mockQueryForSearchClosure());
    }
}
