<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Providers;

use Illuminate\Contracts\Foundation\Application;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\FractalInterface;
use LoyaltyCorp\ApiBlueprint\Providers\ApiBlueprintServiceProvider;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\TestCase;

class ApiBlueprintServiceProviderTest extends TestCase
{
    public function testFractalClosure()
    {
        $getClosure = $this->getMethodAsPublic(
            ApiBlueprintServiceProvider::class,
            'getFractalSingletonClosure'
        );
        /** @var Application $app */
        $app = \app();
        $fractalClosure = $getClosure->invoke(new ApiBlueprintServiceProvider($app));

        self::assertInstanceOf(FractalInterface::class, $fractalClosure());
    }
}
