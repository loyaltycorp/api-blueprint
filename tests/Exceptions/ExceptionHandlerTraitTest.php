<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use LoyaltyCorp\ApiBlueprint\Errors\ErrorsCodes;
use LoyaltyCorp\ApiBlueprint\Exceptions\BaseException;
use LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\LumenExceptionHandler;
use Mockery;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\TestCase;

/**
 * @SuppressWarnings(PHPMD.StaticAccess) Avoid warnings for Mockery
 */
class ExceptionHandlerTraitTest extends TestCase
{
    public function testBaseExceptionGetExtendedMessage()
    {
        $extendedMessage = 'Extended message';
        $exception = new BaseException($extendedMessage, 'code', 400);

        self::assertEquals($extendedMessage, $exception->getExtendedMessage());
    }

    public function testBaseExceptionInDebug()
    {
        \putenv('APP_DEBUG=true');

        $exception = new BaseException('Extended message');
        $handler = new LumenExceptionHandler();

        /** @var JsonResponse $response */
        $response = $handler->render(new Request(), $exception);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([
            'error' => [
                'code' => 0,
                'description' => 'Extended message'
            ]
        ], $response->getData(true));
        self::assertEquals(500, $response->getStatusCode());
    }

    public function testBaseExceptionInProduction()
    {
        \putenv('APP_DEBUG=false');

        $exception = new BaseException('Extended message');
        $handler = new LumenExceptionHandler();

        /** @var JsonResponse $response */
        $response = $handler->render(new Request(), $exception);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([
            'error' => [
                'code' => 0,
                'description' => 'Something went wrong'
            ]
        ], $response->getData(true));
        self::assertEquals(500, $response->getStatusCode());
    }

    public function testBaseExceptionOverrideThroughConstructor()
    {
        \putenv('APP_DEBUG=true');

        $exception = new BaseException('Extended message', 'code', 400);
        $handler = new LumenExceptionHandler();

        /** @var JsonResponse $response */
        $response = $handler->render(new Request(), $exception);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([
            'error' => [
                'code' => 'code',
                'description' => 'Extended message'
            ]
        ], $response->getData(true));
        self::assertEquals(400, $response->getStatusCode());
    }

    public function testBaseExceptionToArray()
    {
        $expected = [
            'message' => 'Something went wrong',
            'extended_message' => 'Extended message',
            'code' => 'code',
            'status_code' => 400
        ];

        $exception = new BaseException(
            (string) $expected['extended_message'],
            $expected['code'],
            (int) $expected['status_code']
        );

        self::assertEquals($expected, $exception->toArray());
    }

    public function testModelNotFoundExceptionToResourceNotFoundException()
    {
        \putenv('APP_DEBUG=true');

        $message = 'Model not found';
        $exception = new ModelNotFoundException($message);

        $handler = new LumenExceptionHandler();

        /** @var JsonResponse $response */
        $response = $handler->render(new Request(), $exception);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([
            'error' => [
                'code' => ErrorsCodes::RESOURCE_NOT_FOUND,
                'description' => $message
            ]
        ], $response->getData(true));
        self::assertEquals(404, $response->getStatusCode());
    }

    public function testNoExceptionInterfaceInDebug()
    {
        \putenv('APP_DEBUG=true');

        $exception = new Exception('Extended message');
        $handler = new LumenExceptionHandler();

        /** @var Response $response */
        $response = $handler->render(new Request(), $exception);

        self::assertInstanceOf(Response::class, $response);
        self::assertEquals(500, $response->getStatusCode());
    }

    public function testNoExceptionInterfaceInProduction()
    {
        \putenv('APP_DEBUG=false');

        $exception = new Exception('Extended message');
        $handler = new LumenExceptionHandler();

        /** @var JsonResponse $response */
        $response = $handler->render(new Request(), $exception);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([
            'error' => [
                'code' => 0,
                'description' => 'Something went wrong'
            ]
        ], $response->getData(true));
        self::assertEquals(500, $response->getStatusCode());
    }

    public function testValidationFailedExceptionErrorsFormatting()
    {
        \putenv('APP_DEBUG=true');

        $exception = Mockery::mock(ValidationException::class);
        $exception->shouldReceive('errors')->once()->withNoArgs()->andReturn([
            'field1' => [0 => 'field1 error'],
            'field2' => [0 => 'field2 error'],
        ]);

        $handler = new LumenExceptionHandler();

        /** @var JsonResponse $response */
        $response = $handler->render(new Request(), $exception);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([
            'error' => [
                'code' => ErrorsCodes::RESOURCE_VALIDATION_FAILED,
                'description' => 'field1: field1 error, field2: field2 error'
            ]
        ], $response->getData(true));
        self::assertEquals(400, $response->getStatusCode());
    }
}
