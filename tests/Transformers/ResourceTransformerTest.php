<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Transformers;

use Illuminate\Support\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\NullResource;
use LoyaltyCorp\ApiBlueprint\Transformers\ResourceTransformer;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ResourceModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\TestCase;
use League\Fractal\Resource\Collection as FractalCollection;

class ResourceTransformerTest extends TestCase
{
    public function testTransformerTransform()
    {
        $attributes = [
            'fillable1' => 'fillable1',
            'fillable2' => 'fillable2',
            'fillable3' => 'fillable3'
        ];

        $model = new ResourceModelStub($attributes);
        $transformer = new ResourceTransformer();

        self::assertEquals($attributes, $transformer->transform($model));
    }

    public function testIncludeCollection()
    {
        $transformer = new ResourceTransformer();
        $includeCollection = $this->getMethodAsPublic(ResourceTransformer::class, 'includeCollection');

        self::assertInstanceOf(FractalCollection::class, $includeCollection->invokeArgs($transformer, [
            new ResourceModelStub(),
            new Collection()
        ]));
    }

    public function testIncludeResource()
    {
        $transformer = new ResourceTransformer();
        $includeCollection = $this->getMethodAsPublic(ResourceTransformer::class, 'includeResource');

        self::assertInstanceOf(Item::class, $includeCollection->invokeArgs($transformer, [
            new ResourceModelStub()
        ]));
    }

    public function testIncludeResourceWithNull()
    {
        $transformer = new ResourceTransformer();
        $includeCollection = $this->getMethodAsPublic(ResourceTransformer::class, 'includeResource');

        self::assertInstanceOf(NullResource::class, $includeCollection->invokeArgs($transformer, [null]));
    }
}
