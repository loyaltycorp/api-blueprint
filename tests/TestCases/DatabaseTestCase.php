<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\TestCases;

use Illuminate\Contracts\Console\Kernel as ConsoleKernel;
use Illuminate\Contracts\Debug\ExceptionHandler as DebugExceptionHandler;
use Laravel\Lumen\Application;
use Laravel\Lumen\Console\Kernel;
use Laravel\Lumen\Testing\DatabaseTransactions;
use LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\LumenExceptionHandler;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\StrInterface;
use Mockery;
use ReflectionException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class DatabaseTestCase extends TestCase
{
    use DatabaseTransactions;

    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return HttpKernelInterface
     */
    public function createApplication()
    {
        $application = parent::createApplication();

        $application->withEloquent();
        $application->singleton(ConsoleKernel::class, Kernel::class);
        $application->singleton(DebugExceptionHandler::class, LumenExceptionHandler::class);

        \config(['database.default' => 'testing']);

        return $application;
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate', ['--path' => '../migrations']);
    }

    public function tearDown(): void
    {
        parent::tearDown();

        $this->beforeApplicationDestroyed(function () {
            $this->artisan('migrate:rollback', ['--path' => '../migrations']);
        });
    }

    /**
     * Override StrInterface in container with mock to throw ReflectionException.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    protected function registerStrForReflectionException()
    {
        \app()->singleton(StrInterface::class, function () {
            $str = Mockery::mock(StrInterface::class);
            $str->shouldReceive('lower')->once()->andThrow(ReflectionException::class);

            return $str;
        });
    }
}
