<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\TestCases;

use Illuminate\Contracts\Console\Kernel;
use LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\LumenExceptionHandler;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Console\ConsoleKernelStub;
use Illuminate\Contracts\Debug\ExceptionHandler as DebugExceptionHandler;

class CommandsTestCase extends TestCase
{
    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return HttpKernelInterface
     */
    public function createApplication()
    {
        $application = parent::createApplication();
        $application->withFacades();
        $application->singleton(Kernel::class, ConsoleKernelStub::class);
        $application->singleton(DebugExceptionHandler::class, LumenExceptionHandler::class);


        return $application;
    }
}
