<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\TestCases;

use Illuminate\Http\Request;
use League\Fractal\Serializer\JsonApiSerializer;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\FractalInterface;
use LoyaltyCorp\ApiBlueprint\External\Libraries\Fractal;

class ControllersTestCase extends DatabaseTestCase
{
    /**
     * @return FractalInterface
     */
    protected function getFractal(): FractalInterface
    {
        return (new Fractal())->setSerializer(new JsonApiSerializer());
    }

    /**
     * @param array $query
     *
     * @return Request
     */
    protected function getRequest(array $query = null): Request
    {
        return new Request($query ?? []);
    }
}
