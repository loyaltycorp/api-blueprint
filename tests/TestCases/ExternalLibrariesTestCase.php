<?php declare(strict_types=1);

namespace Tests\LoyaltyCorp\ApiBlueprint\TestCases;

class ExternalLibrariesTestCase extends TestCase
{
    public function createApplication()
    {
        $application = parent::createApplication();
        $application->withFacades();

        return $application;
    }
}
