<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\TestCases;

use Laravel\Lumen\Application;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use LoyaltyCorp\ApiBlueprint\Providers\ApiBlueprintServiceProvider;
use ReflectionClass;
use ReflectionMethod;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class TestCase extends BaseTestCase
{
    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return HttpKernelInterface
     */
    public function createApplication()
    {
        /** @var HttpKernelInterface $application */
        $application = new Application(\realpath(__DIR__));

        $application->register(ApiBlueprintServiceProvider::class);

        return $application;
    }

    /**
     * Build an array $key => $value from simple array of strings.
     *
     * @param array $strings
     *
     * @return array
     */
    protected function arrayFromStrings(array $strings): array
    {
        $return = [];

        foreach ($strings as $string) {
            $return[$string] = $string;
        }

        return $return;
    }

    /**
     * Convert protected/private method to public.
     *
     * @param string $class
     * @param string $methodName
     *
     * @return ReflectionMethod
     */
    protected function getMethodAsPublic(string $class, string $methodName): ReflectionMethod
    {
        $reflection = new ReflectionClass($class);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }
}
