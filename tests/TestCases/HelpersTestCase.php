<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\TestCases;

use Closure;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;
use LoyaltyCorp\ApiBlueprint\Helpers\QueryModifier;
use Mockery;
use Mockery\MockInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class HelpersTestCase extends TestCase
{
    /** @var array */
    public static $resourceFillables = ['fillable1', 'fillable2', 'fillable3', 'fillable4'];

    /**
     * @return QueryModifier
     */
    protected function getQueryModifier(): QueryModifier
    {
        return new QueryModifier(static::$resourceFillables, 'id,asc', static::$resourceFillables);
    }

    /**
     * Mock query for ResourceModifierInterface::getSearchClosure test.
     *
     * @return MockInterface
     */
    protected function mockQueryForSearchClosure(): MockInterface
    {
        $query = Mockery::mock(Builder::class);

        foreach (static::$resourceFillables as $field) {
            $query->shouldReceive('orWhere')->once()->with($field, 'like', '%search%')->andReturnSelf();
        }

        return $query;
    }

    /**
     * Mock resource for ResourceModifierInterface::applyFilters test.
     *
     * @return MockInterface
     */
    protected function mockResourceForFilters(): MockInterface
    {
        $resource = Mockery::mock(ResourceModel::class);
        $resource->shouldReceive('where')->once()->withArgs(['fillable1', '>', '1', 'and'])->andReturnSelf();
        $resource->shouldReceive('where')->once()->withArgs([['fillable2' => '1']])->andReturnSelf();
        $resource->shouldReceive('whereIn')->once()->withArgs(['fillable3', ['1', '2', '3']])->andReturnSelf();
        $resource->shouldReceive('where')->once()->withArgs([['fillable4' => '1']])->andReturnSelf();

        return $resource;
    }

    /**
     * Mock resource for ResourceModifierInterface::applyOrderBy test.
     *
     * @return MockInterface
     */
    protected function mockResourceForOrderBy(): MockInterface
    {
        $resource = Mockery::mock(ResourceModel::class);
        $resource->shouldReceive('orderBy')->once()->withArgs(['fillable1', 'desc'])->andReturnSelf();

        return $resource;
    }

    /**
     * Mock resource for ResourceModifierInterface::applyOrderBy test with invalid field.
     *
     * @return MockInterface
     */
    protected function mockResourceForOrderByWithInvalidField(): MockInterface
    {
        return Mockery::mock(ResourceModel::class);
    }

    /**
     * Mock resource for ResourceModifierInterface::applySearch test.
     *
     * @return MockInterface
     */
    protected function mockResourceForSearch(): MockInterface
    {
        $resource = Mockery::mock(ResourceModel::class);
        $resource->shouldReceive('where')->once()->withArgs([Mockery::type(Closure::class)])->andReturnSelf();

        return $resource;
    }

    /**
     * Mock resource for ResourceModifierInterface::applySearch test with null parameter.
     *
     * @return MockInterface
     */
    protected function mockResourceForSearchWithNull(): MockInterface
    {
        $resource = Mockery::mock(ResourceModel::class);
        $resource->shouldNotReceive('where');

        return $resource;
    }
}
