<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\External\Libraries;

use LoyaltyCorp\ApiBlueprint\External\Libraries\Str;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\ExternalLibrariesTestCase;

class StrTest extends ExternalLibrariesTestCase
{
    /** @var string */
    private $message = 'API Blueprint is Amazing!';

    public function testContains()
    {
        $trues = ['is'];
        $falses = ['nathan'];

        $str = new Str();

        foreach ($trues as $true) {
            self::assertTrue($str->contains($this->message, $true));
        }

        foreach ($falses as $false) {
            self::assertNotTrue($str->contains($this->message, $false));
        }
    }

    public function testEndsWith()
    {
        $trues = ['!', 'zing!'];
        $falses = ['nathan', 'API'];

        $str = new Str();

        foreach ($trues as $true) {
            self::assertTrue($str->endsWith($this->message, $true));
        }

        foreach ($falses as $false) {
            self::assertNotTrue($str->endsWith($this->message, $false));
        }
    }

    public function testLower()
    {
        $expected = 'api blueprint is amazing!';

        self::assertEquals($expected, (new Str())->lower($this->message));
    }

    public function testSlug()
    {
        $expected = 'api-blueprint-is-amazing';

        self::assertEquals($expected, (new Str())->slug($this->message));
    }

    public function testSnake()
    {
        $expected = 'a_p_i_blueprint_is_amazing!';

        self::assertEquals($expected, (new Str())->snake($this->message));
    }

    public function testStartsWith()
    {
        $trues = ['A', 'API', 'API B'];
        $falses = ['nathan', '!', 'zing!'];

        $str = new Str();

        foreach ($trues as $true) {
            self::assertTrue($str->startsWith($this->message, $true));
        }

        foreach ($falses as $false) {
            self::assertNotTrue($str->startsWith($this->message, $false));
        }
    }
}
