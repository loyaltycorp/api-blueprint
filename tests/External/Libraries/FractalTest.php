<?php declare(strict_types=1);

namespace Tests\LoyaltyCorp\ApiBlueprint\External\Libraries;

use Illuminate\Support\Collection;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item;
use League\Fractal\Scope;
use League\Fractal\Serializer\JsonApiSerializer;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;
use LoyaltyCorp\ApiBlueprint\External\Libraries\Fractal;
use LoyaltyCorp\ApiBlueprint\Transformers\ResourceTransformer;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ResourceModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\ExternalLibrariesTestCase;

class FractalTest extends ExternalLibrariesTestCase
{
    public function testCreateCollection()
    {
        self::assertInstanceOf(
            FractalCollection::class,
            (new Fractal())->createCollection(new Collection(), new ResourceTransformer(), 'resource')
        );
    }

    public function testCreateData()
    {
        $fractal = new Fractal();

        self::assertInstanceOf(Scope::class, $fractal->createData(new Item()));
        self::assertInstanceOf(Scope::class, $fractal->createData(new Item(), 'scope'));
        self::assertInstanceOf(
            Scope::class,
            $fractal->createData(new Item(), 'scope', new Scope($fractal, new Item(), 'scope_parent'))
        );
    }

    public function testCreateItem()
    {
        self::assertInstanceOf(
            Item::class,
            (new Fractal())->createItem(new ResourceModelStub(), new ResourceTransformer(), 'resource')
        );
    }

    public function testParseExcludes()
    {
        $fractal = new Fractal();

        self::assertInstanceOf(Fractal::class, $fractal->parseExcludes('exclude1,exclude2'));
        self::assertInstanceOf(Fractal::class, $fractal->parseExcludes(['exclude1', 'exclude2']));
    }

    public function testParseIncludes()
    {
        $fractal = new Fractal();

        self::assertInstanceOf(Fractal::class, $fractal->parseIncludes('include1,include2'));
        self::assertInstanceOf(Fractal::class, $fractal->parseIncludes(['include1', 'include2']));
    }

    public function testParseIncludesAndExcludes()
    {
        $fractal = new Fractal();

        self::assertInstanceOf(Fractal::class, $fractal->parseIncludesAndExcludes(
            'include1,include2',
            'exclude1,exclude2'
        ));
        self::assertInstanceOf(Fractal::class, $fractal->parseIncludesAndExcludes(
            ['include1', 'include2'],
            ['exclude1', 'exclude2']
        ));
    }

    public function testSetSerializer()
    {
        self::assertInstanceOf(Fractal::class, (new Fractal())->setSerializer(new JsonApiSerializer()));
    }
}
