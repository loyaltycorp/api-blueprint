<?php declare(strict_types=1);

namespace Tests\LoyaltyCorp\ApiBlueprint\External\Libraries;

use LoyaltyCorp\ApiBlueprint\External\Libraries\Inflector;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\ExternalLibrariesTestCase;

class InflectorTest extends ExternalLibrariesTestCase
{
    public function testPluralize()
    {
        $plurals = [
            'category' => 'categories',
            'house' => 'houses',
            'product' => 'products',
            'product_order' => 'product_orders'
        ];

        $inflector = new Inflector();

        foreach ($plurals as $singular => $plural) {
            self::assertEquals($plural, $inflector->pluralize($singular));
        }
    }
}
