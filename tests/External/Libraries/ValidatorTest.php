<?php declare(strict_types=1);

namespace Tests\LoyaltyCorp\ApiBlueprint\External\Libraries;

use Illuminate\Validation\Validator as IlluminateValidator;
use LoyaltyCorp\ApiBlueprint\External\Libraries\Validator;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\ExternalLibrariesTestCase;

class ValidatorTest extends ExternalLibrariesTestCase
{
    public function testMake()
    {
        self::assertInstanceOf(IlluminateValidator::class, (new Validator())->make([], []));
    }
}
