<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Controllers;

use Illuminate\Http\JsonResponse;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ValidationFailedException;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Controllers\DeleteExceptionControllerStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Controllers\LaravelResourceControllerStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Controllers\ResourceControllerStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\DeleteExceptionStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ResourceModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\ControllersTestCase;

class ResourceControllerTest extends ControllersTestCase
{
    public function testDestroy()
    {
        $resource = new ResourceModelStub($this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']));
        $resource->save();

        $controller = new ResourceControllerStub($this->getRequest(), $this->getFractal());
        $response = $controller->destroy($resource->id);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([], $response->getData(true));
    }

    public function testDestroyWithException()
    {
        $this->expectException(ResourceErrorException::class);

        $resource = new DeleteExceptionStub($this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']));
        $resource->save();

        $controller = new DeleteExceptionControllerStub($this->getRequest(), $this->getFractal());
        $controller->destroy($resource->id);
    }

    public function testIndex()
    {
        $controller = new ResourceControllerStub($this->getRequest(), $this->getFractal());
        $response = $controller->index();

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([
            'data' => [],
            'meta' => [
                'pagination' => [
                    'total' => 0,
                    'count' => 0,
                    'per_page' => 15,
                    'current_page' => 1,
                    'total_pages' => 1
                ]
            ],
            'links' => [
                'self' => 'http://:?page=1',
                'first' => 'http://:?page=1',
                'last' => 'http://:?page=1'
            ]
        ], $response->getData(true));
    }

    public function testRepositoryIsNotInstantiatedTwice()
    {
        $controller = new ResourceControllerStub($this->getRequest(), $this->getFractal());
        $getRepository = $this->getMethodAsPublic(ResourceControllerStub::class, 'getRepository');

        $repository1 = $getRepository->invoke($controller);
        $repository2 = $getRepository->invoke($controller);

        self::assertEquals(\spl_object_hash($repository1), \spl_object_hash($repository2));
    }

    public function testShow()
    {
        $resource = new ResourceModelStub($this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']));
        $resource->save();

        $controller = new ResourceControllerStub($this->getRequest(), $this->getFractal());
        $response = $controller->show($resource->id);

        $attributes = $resource->toArray();
        unset($attributes['id']);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([
            'data' => [
                'type' => 'resourcemodelstub',
                'id' => $resource->id,
                'attributes' => $attributes
            ]
        ], $response->getData(true));
    }

    public function testStore()
    {
        $query = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $controller = new ResourceControllerStub($this->getRequest($query), $this->getFractal());
        $response = $controller->store();
        $data = $response->getData(true);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertArrayHasKey('data', $data);
        self::assertEquals('resourcemodelstub', $data['data']['type']);
        self::assertArrayHasKey('id', $data['data']);
        self::assertArrayHasKey('attributes', $data['data']);
        self::assertArrayHasKey('fillable1', $data['data']['attributes']);
        self::assertArrayHasKey('fillable2', $data['data']['attributes']);
        self::assertArrayHasKey('fillable3', $data['data']['attributes']);
    }

    public function testStoreWithValidationFailedException()
    {
        $this->expectException(ValidationFailedException::class);

        $controller = new ResourceControllerStub($this->getRequest(), $this->getFractal());
        $controller->store();
    }

    public function testStoreWithValidationFailedExceptionAndLaravelController()
    {
        $this->expectException(ValidationFailedException::class);

        $controller = new LaravelResourceControllerStub($this->getRequest(), $this->getFractal());
        $controller->store();
    }

    public function testUpdate()
    {
        $attributes = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);
        $updatedAttributes = [
            'fillable1' => 'fillable2',
            'fillable2' => 'fillable1',
            'fillable3' => 'fillable3'
        ];

        $resource = new ResourceModelStub($attributes);
        $resource->save();

        $controller = new ResourceControllerStub($this->getRequest($updatedAttributes), $this->getFractal());
        $response = $controller->update($resource->id);

        $updatedAttributes = $resource->refresh()->toArray();
        unset($updatedAttributes['id']);

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals([
            'data' => [
                'type' => 'resourcemodelstub',
                'id' => $resource->id,
                'attributes' => $updatedAttributes
            ]
        ], $response->getData(true));
    }

    public function testUpdateWithValidationFailedException()
    {
        $this->expectException(ValidationFailedException::class);

        $attributes = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);
        $resource = new ResourceModelStub($attributes);
        $resource->save();

        $controller = new ResourceControllerStub($this->getRequest(['fillable1' => null]), $this->getFractal());
        $controller->update($resource->id);
    }
}
