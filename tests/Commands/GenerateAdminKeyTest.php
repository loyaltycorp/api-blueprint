<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Commands;

use Illuminate\Support\Facades\Artisan;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\CommandsTestCase;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class GenerateAdminKeyTest extends CommandsTestCase
{
    public function testCommand()
    {
        Artisan::call('api:generate-admin-key');

        self::assertStringStartsWith('New admin api key: ', Artisan::output());
    }
}
