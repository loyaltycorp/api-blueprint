<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Services;

use LoyaltyCorp\ApiBlueprint\Services\Keys\KeyGenerator;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\TestCase;

class KeyGeneratorTest extends TestCase
{
    public function testWithNoPrefix()
    {
        $keyGenerator = new KeyGenerator();

        $key = $keyGenerator->generate('key');

        self::assertTrue(\is_string($key));
        self::assertEquals(32, \strlen($key));
    }

    public function testWithPrefix()
    {
        $keyGenerator = new KeyGenerator();

        $key = $keyGenerator->generate('key', 'prefix_');

        self::assertTrue(\is_string($key));
        self::assertEquals(39, \strlen($key));
        self::assertStringStartsWith('prefix_', $key);
    }
}
