<?php declare(strict_types=1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Database\Models;

use LoyaltyCorp\ApiBlueprint\Exceptions\BaseException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ValidationFailedException;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\AutoIncrementModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\InvalidValidationRulesStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ParentModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ResourceModelEmptyStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ResourceModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Transformers\ParentModelTransformerStub;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\DatabaseTestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ResourceModelTest extends DatabaseTestCase
{
    public function testAddChild()
    {
        $model = new ResourceModelStub();
        $child = new ResourceModelStub();

        $model->id = 'id';
        $model->addChild($child, 'custom_foreign_key');

        self::assertEquals('id', $child->custom_foreign_key);
    }

    public function testAddChildWithNullForeignKey()
    {
        $model = new ParentModelStub();
        $child = new ResourceModelStub();

        $model->id = 'id';
        $model->addChild($child);

        self::assertEquals('id', $child->parent_model_stub_id);
    }

    public function testAddChildren()
    {
        $model = new ResourceModelStub();
        $children = [
            new ResourceModelStub(),
            new ResourceModelStub(),
            new ResourceModelStub()
        ];

        $model->id = 'id';
        $model->addChildren($children, 'custom_foreign_key');

        foreach ($children as $child) {
            self::assertEquals('id', $child->custom_foreign_key);
        }
    }

    public function testAddChildrenWithNullForeignKey()
    {
        $model = new ParentModelStub();
        $children = [
            new ResourceModelStub(),
            new ResourceModelStub(),
            new ResourceModelStub()
        ];

        $model->id = 'id';
        $model->addChildren($children);

        foreach ($children as $child) {
            self::assertEquals('id', $child->parent_model_stub_id);
        }
    }

    public function testConfigurationFields()
    {
        $model = new ResourceModelStub();

        $searchFields = ['fillable1'];
        $filterFields = ['fillable2'];
        $orderByAndDirection = 'fillable3,desc';

        self::assertEquals($searchFields, $model->getSearchFields());
        self::assertEquals($filterFields, $model->getFilterFields());
        self::assertEquals($orderByAndDirection, $model->getOrderByAndDirection());
    }

    public function testCustomTransformerInstance()
    {
        $model = new ParentModelStub();

        self::assertInstanceOf(ParentModelTransformerStub::class, $model->getTransformer());
    }

    public function testEmptyConfigurationFields()
    {
        $model = new ResourceModelEmptyStub();

        $searchFields = ['fillable1', 'fillable2', 'fillable3'];
        $filterFields = ['fillable1', 'fillable2', 'fillable3', 'id'];
        $orderByAndDirection = 'id,asc';

        self::assertEquals($searchFields, $model->getSearchFields());
        self::assertEquals($filterFields, $model->getFilterFields());
        self::assertEquals($orderByAndDirection, $model->getOrderByAndDirection());
    }

    public function testEmptyValidationRules()
    {
        $this->expectException(BaseException::class);

        $model = new ResourceModelEmptyStub();

        $model->validate('default');
    }

    public function testGetResourceForeignKey()
    {
        $model = new ResourceModelStub();

        self::assertEquals('resource_model_stub_id', $model->getResourceForeignKey());
    }

    public function testGetResourceKeys()
    {
        $model = new ResourceModelStub();

        self::assertEquals('resourcemodelstub', $model->getResourceItemKey());
        self::assertEquals('resourcemodelstubs', $model->getResourceCollectionKey());
    }

    public function testGetResourceItemKeyWithReflectionException()
    {
        $this->expectException(ResourceErrorException::class);

        $this->registerStrForReflectionException();

        $model = new ResourceModelStub();

        self::assertEquals('testmodelstub', $model->getResourceItemKey());
    }

    public function testGetResourceLimit()
    {
        $model = new ResourceModelStub();

        self::assertEquals(15, $model->getResourceLimit());
    }

    public function testGetResourcePrimaryKey()
    {
        $model = new ResourceModelStub();

        self::assertEquals('id', $model->getResourcePrimaryKey());
    }

    public function testInvalidValidationRules()
    {
        $this->expectException(BaseException::class);

        $model = new InvalidValidationRulesStub();

        $model->validate('default');
    }

    public function testModelInstance()
    {
        self::assertInstanceOf(ResourceInterface::class, new ResourceModelStub());
    }

    public function testSaveWithSameIdException()
    {
        $this->expectException(ResourceErrorException::class);

        $model1 = new ResourceModelStub(['fillable1' => 'fillable1']);
        $model1->save();

        $model2 = new ResourceModelStub(['fillable1' => 'fillable1']);
        $model2->save();

        $model2->id = $model1->id;
        $model2->save();
    }

    public function testToArray()
    {
        $toArray = (new ResourceModelStub())->toArray();

        self::assertTrue(\is_array($toArray));
        self::assertCount(0, $toArray);
    }

    public function testTransformerInstance()
    {
        $model = new ResourceModelStub();

        self::assertInstanceOf(ResourceTransformerInterface::class, $model->getTransformer());
    }

    public function testValidationFailed()
    {
        $this->expectException(ValidationFailedException::class);

        $model = new ResourceModelStub();

        $model->validate('default');
    }

    public function testValidationSuccess()
    {
        $model = new ResourceModelStub([
            'fillable1' => 'fillable1',
            'fillable2' => 'fillable2',
            'fillable3' => 'fillable3'
        ]);

        self::assertTrue($model->validate('default'));
    }

    public function testAutoIncrementIdStrategy(): void
    {
        $model = new AutoIncrementModelStub($this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']));
        $model->save();

        $model2 = new AutoIncrementModelStub($this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']));
        $model2->save();

        self::assertInternalType('int', $model->id);
        self::assertEquals(1, $model->id);

        self::assertInternalType('int', $model2->id);
        self::assertEquals(2, $model2->id);
    }
}
