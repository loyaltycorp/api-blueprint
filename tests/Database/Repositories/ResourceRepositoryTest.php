<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Database\Repositories;

use LoyaltyCorp\ApiBlueprint\Database\Collections\ResourceCollection;
use LoyaltyCorp\ApiBlueprint\Exceptions\InvalidRelationException;
use LoyaltyCorp\ApiBlueprint\Exceptions\InvalidResourceException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceNotFoundException;
use LoyaltyCorp\ApiBlueprint\Helpers\PaginatedResponseBuilder;
use LoyaltyCorp\ApiBlueprint\Helpers\QueryModifier;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceRepositoryInterface;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ChildBelongsToManyModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ChildModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ParentBelongsToManyModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ParentModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ResourceModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\BelongsToManyRelationRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\FillBaseExceptionRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\FillMassAssignmentExceptionRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\IndexInvalidArgumentRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\InvalidCollectionRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\ParentInvalidRelationRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\ParentInvalidRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\ParentNullRelationRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\ParentRelationRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\ResourceInvalidRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\ResourceRepositoryStub;
use Tests\LoyaltyCorp\ApiBlueprint\TestCases\DatabaseTestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ResourceRepositoryTest extends DatabaseTestCase
{
    public function testAllEmpty()
    {
        $all = (new ResourceRepositoryStub())->all();

        self::assertInstanceOf(ResourceCollection::class, $all);
        self::assertEquals(0, $all->count());
    }

    public function testAllWithInvalidCollectionInstance()
    {
        $this->expectException(InvalidResourceException::class);

        (new InvalidCollectionRepositoryStub())->all();
    }

    public function testDelete()
    {
        $this->expectException(ResourceNotFoundException::class);

        $model = new ResourceModelStub($this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']));
        $model->save();

        $repository = (new ResourceRepositoryStub())->setCurrentPrimaryKey($model->id);
        $repository->delete($model);
        $repository->get();
    }

    public function testDeleteWithBelongsToMany()
    {
        $this->expectException(ResourceNotFoundException::class);

        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentBelongsToManyModelStub($fillables);
        $parent->save();

        $child = new ChildBelongsToManyModelStub($fillables);
        $child->save();

        $repository = (new BelongsToManyRelationRepositoryStub())
            ->setParentPrimaryKey($parent->id)
            ->setCurrentPrimaryKey($child->id);

        $repository->save($child);
        $repository->delete($child);
        $repository->get();
    }

    public function testFill()
    {
        self::assertInstanceOf(ResourceModelStub::class, (new ResourceRepositoryStub())->fill(
            $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3'])
        ));
    }

    public function testFillBaseException()
    {
        $this->expectException(ResourceErrorException::class);

        (new FillBaseExceptionRepositoryStub())->fill([]);
    }

    public function testFillMassAssignmentException()
    {
        $this->expectException(ResourceErrorException::class);

        (new FillMassAssignmentExceptionRepositoryStub())->fill([]);
    }

    public function testGet()
    {
        $model = new ResourceModelStub($this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']));
        $model->save();

        $get = (new ResourceRepositoryStub())->setCurrentPrimaryKey($model->id)->get();

        self::assertInstanceOf(ResourceModelStub::class, $get);
        self::assertEquals($model->id, $get->id);
    }

    public function testGetWithInvalidResource()
    {
        $this->expectException(InvalidResourceException::class);

        $model = new ResourceModelStub($this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']));
        $model->save();

        (new ResourceInvalidRepositoryStub())->setCurrentPrimaryKey($model->id)->get();
    }

    public function testGetResourceNotFound()
    {
        $this->expectException(ResourceNotFoundException::class);

        (new ResourceRepositoryStub())->get();
    }

    public function testGetWithNullParentClass()
    {
        $this->expectException(InvalidResourceException::class);

        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentModelStub($fillables);
        $parent->save();

        $child = new ChildModelStub($fillables);
        $parent->addChild($child);
        $child->save();

        (new ResourceRepositoryStub())
            ->setParentPrimaryKey($parent->id)
            ->setCurrentPrimaryKey($child->id)
            ->get();
    }

    public function testGetWithInvalidParentClass()
    {
        $this->expectException(InvalidResourceException::class);

        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentModelStub($fillables);
        $parent->save();

        $child = new ChildModelStub($fillables);
        $parent->addChild($child);
        $child->save();

        (new ParentInvalidRepositoryStub())
            ->setParentPrimaryKey($parent->id)
            ->setCurrentPrimaryKey($child->id)
            ->get();
    }

    public function testGetWithInvalidParentRelation()
    {
        $this->expectException(InvalidRelationException::class);

        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentModelStub($fillables);
        $parent->save();

        $child = new ChildModelStub($fillables);
        $parent->addChild($child);
        $child->save();

        (new ParentInvalidRelationRepositoryStub())
            ->setParentPrimaryKey($parent->id)
            ->setCurrentPrimaryKey($child->id)
            ->get();
    }

    public function testGetWithNullParentRelation()
    {
        $this->expectException(InvalidRelationException::class);

        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentModelStub($fillables);
        $parent->save();

        $child = new ChildModelStub($fillables);
        $parent->addChild($child);
        $child->save();

        (new ParentNullRelationRepositoryStub())
            ->setParentPrimaryKey($parent->id)
            ->setCurrentPrimaryKey($child->id)
            ->get();
    }

    public function testGetWithParent()
    {
        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentModelStub($fillables);
        $parent->save();

        $child = new ChildModelStub($fillables);
        $parent->addChild($child);
        $child->save();

        $get = (new ParentRelationRepositoryStub())
            ->setParentPrimaryKey($parent->id)
            ->setCurrentPrimaryKey($child->id)
            ->get();

        self::assertInstanceOf(ChildModelStub::class, $get);
        self::assertEquals($child->id, $get->id);
    }

    public function testGetWithParentNotFound()
    {
        $this->expectException(ResourceNotFoundException::class);

        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentModelStub($fillables);
        $parent->save();

        $child = new ChildModelStub($fillables);
        $parent->addChild($child);
        $child->save();

        (new ParentRelationRepositoryStub())
            ->setParentPrimaryKey('invalid')
            ->setCurrentPrimaryKey($child->id)
            ->get();
    }

    public function testGetWithoutParent()
    {
        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentBelongsToManyModelStub($fillables);
        $parent->save();

        $child = new ChildBelongsToManyModelStub($fillables);
        $child->save();

        $get = (new BelongsToManyRelationRepositoryStub())
            ->setParentPrimaryKey($parent->id)
            ->setCurrentPrimaryKey($child->id)
            ->getWithoutParent();

        self::assertInstanceOf(ChildBelongsToManyModelStub::class, $get);
        self::assertEquals($child->id, $get->id);
    }

    public function testIndexEmpty()
    {
        $index = (new ResourceRepositoryStub())->index();

        self::assertInstanceOf(PaginatedResponseBuilder::class, $index);
        self::assertEquals(0, $index->getPaginator()->getCollection()->count());
    }

    public function testIndexInvalidArgument()
    {
        $this->expectException(ResourceErrorException::class);

        (new IndexInvalidArgumentRepositoryStub())->index();
    }

    public function testIndexWithCustomQueryModifier()
    {
        $index = (new ResourceRepositoryStub())->setQueryModifier(new QueryModifier([], '', []))->index();

        self::assertInstanceOf(PaginatedResponseBuilder::class, $index);
        self::assertEquals(0, $index->getPaginator()->getCollection()->count());
    }

    public function testSave()
    {
        $model = new ResourceModelStub($this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']));

        (new ResourceRepositoryStub())->save($model);

        self::assertNotNull($model->id);
    }

    public function testSaveWithBelongsToManyRelation()
    {
        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentBelongsToManyModelStub($fillables);
        $parent->save();

        $child = new ChildBelongsToManyModelStub($fillables);
        $child->save();

        $save = (new BelongsToManyRelationRepositoryStub())
            ->setParentPrimaryKey($parent->id)
            ->setCurrentPrimaryKey($child->id)
            ->save($child, $this->arrayFromStrings(['extra']));

        self::assertTrue($save);
        self::assertTrue($parent->refresh()->children->contains($child));
        self::assertEquals('extra', $parent->children->first()->pivot->extra);
    }

    public function testSaveWithBelongsToManyRelationMultipleTimes()
    {
        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentBelongsToManyModelStub($fillables);
        $parent->save();

        $child1 = new ChildBelongsToManyModelStub($fillables);
        $child1->save();

        $child2 = new ChildBelongsToManyModelStub($fillables);
        $child2->save();

        foreach ([$child1, $child2] as $child) {
            (new BelongsToManyRelationRepositoryStub())
                ->setParentPrimaryKey($parent->id)
                ->setCurrentPrimaryKey($child->id)
                ->save($child);
        }

        self::assertEquals(2, $parent->children->count());
    }

    public function testSaveWithParent()
    {
        $fillables = $this->arrayFromStrings(['fillable1', 'fillable2', 'fillable3']);

        $parent = new ParentModelStub($fillables);
        $parent->save();

        $child = new ChildModelStub($fillables);

        $save = (new ParentRelationRepositoryStub())
            ->setParentPrimaryKey($parent->id)
            ->save($child);

        self::assertNotNull($child->id);
        self::assertTrue($save);
    }

    public function testSetCurrentPrimaryKey()
    {
        self::assertInstanceOf(
            ResourceRepositoryInterface::class,
            (new ResourceRepositoryStub())->setCurrentPrimaryKey('primaryKey')
        );
    }

    public function testSetParentPrimaryKey()
    {
        self::assertInstanceOf(
            ResourceRepositoryInterface::class,
            (new ResourceRepositoryStub())->setParentPrimaryKey('primaryKey')
        );
    }

    public function testGetCurrentAndParentPrimaryKeys()
    {
        $primaryKey = 'primaryKey';
        $parentPrimaryKey = 'parentPrimaryKey';

        $repository = (new ResourceRepositoryStub())
            ->setCurrentPrimaryKey($primaryKey)
            ->setParentPrimaryKey($parentPrimaryKey);

        self::assertEquals($primaryKey, $repository->getCurrentPrimaryKey());
        self::assertEquals($parentPrimaryKey, $repository->getParentPrimaryKey());
    }

    public function testSetQueryModifier()
    {
        self::assertInstanceOf(
            ResourceRepositoryInterface::class,
            (new ResourceRepositoryStub())->setQueryModifier(new QueryModifier([], '', []))
        );
    }
}
