<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

use LoyaltyCorp\ApiBlueprint\Exceptions\BaseException;

class FillBaseExceptionStub extends ResourceModelStub
{
    /**
     * @param array $attributes
     *
     * @return void
     *
     * @throws BaseException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fill(array $attributes = []): void
    {
        throw new BaseException();
    }
}
