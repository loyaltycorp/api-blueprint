<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

class IndexInvalidArgumentStub extends ResourceModelStub
{
    /**
     * @param null   $perPage
     * @param array  $columns
     * @param string $pageName
     * @param null   $page
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null): void
    {
        throw new \InvalidArgumentException('');
    }
}
