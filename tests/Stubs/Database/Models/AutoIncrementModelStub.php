<?php
declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

use LoyaltyCorp\ApiBlueprint\Database\IdStrategies\AutoIncrementStrategy;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceIdStrategyInterface;

class AutoIncrementModelStub extends ResourceModelStub
{
    /**
     * Get resource ID strategy.
     *
     * @return ResourceIdStrategyInterface
     */
    public function getResourceIdStrategy(): ResourceIdStrategyInterface
    {
        return new AutoIncrementStrategy();
    }
}
