<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

class DeleteExceptionStub extends ResourceModelStub
{
    /**
     * @throws \Exception
     */
    public function delete()
    {
        throw new \Exception('');
    }
}
