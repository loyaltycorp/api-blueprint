<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;

class ResourceModelStub extends ResourceModel
{
    protected $fillable = [
        'fillable1',
        'fillable2',
        'fillable3'
    ];

    public function getFilterFields(): array
    {
        return ['fillable2'];
    }

    public function getOrderByAndDirection(): string
    {
        return 'fillable3,desc';
    }

    public function getSearchFields(): array
    {
        return ['fillable1'];
    }

    /**
     * Return array of validation rules sets following Laravel Validation rules format.
     *
     * @return array
     */
    protected function getValidationRules(): array
    {
        return [
            'default' => [
                'fillable1' => 'required|string',
                'fillable2' => 'required|string',
                'fillable3' => 'required|string'
            ]
        ];
    }
}
