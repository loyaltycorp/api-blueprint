<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Transformers\ParentModelTransformerStub;

class ParentModelStub extends ResourceModel
{
    protected $fillable = [
        'fillable1',
        'fillable2',
        'fillable3'
    ];

    /**
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(ChildModelStub::class);
    }

    /**
     * @return ResourceTransformerInterface
     */
    public function getTransformer(): ResourceTransformerInterface
    {
        return new ParentModelTransformerStub();
    }

    /**
     * Return array of validation rules sets following Laravel Validation rules format.
     *
     * @return array
     */
    protected function getValidationRules(): array
    {
        return [
            'default' => [
                'fillable1' => 'required|string',
                'fillable2' => 'required|string',
                'fillable3' => 'required|string'
            ]
        ];
    }
}
