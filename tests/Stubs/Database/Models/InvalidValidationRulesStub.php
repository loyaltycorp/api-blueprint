<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;

class InvalidValidationRulesStub extends ResourceModel
{
    protected $fillable = [
        'fillable1'
    ];

    /**
     * Return array of validation rules sets following Laravel Validation rules format.
     *
     * @return array
     */
    protected function getValidationRules(): array
    {
        return [];
    }
}
