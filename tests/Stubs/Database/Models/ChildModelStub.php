<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ChildModelStub extends ResourceModelStub
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(ParentModelStub::class);
    }
}
