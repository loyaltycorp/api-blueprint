<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

use Illuminate\Database\Eloquent\MassAssignmentException;

class FillMassAssignmentExceptionStub extends ResourceModelStub
{
    /**
     * @param array $attributes
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fill(array $attributes): void
    {
        throw new MassAssignmentException('');
    }
}
