<?php
declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ChildBelongsToManyModelStub extends ResourceModelStub
{
    /**
     * @return BelongsToMany
     */
    public function parents(): BelongsToMany
    {
        return $this->belongsToMany(ParentBelongsToManyModelStub::class)
            ->withPivot(['extra'])
            ->withTimestamps();
    }
}
