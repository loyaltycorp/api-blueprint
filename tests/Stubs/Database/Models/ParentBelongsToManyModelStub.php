<?php
declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ParentBelongsToManyModelStub extends ResourceModelStub
{
    /**
     * @return BelongsToMany
     */
    public function children(): BelongsToMany
    {
        return $this->belongsToMany(ChildBelongsToManyModelStub::class)
            ->withPivot(['extra'])
            ->withTimestamps();
    }
}
