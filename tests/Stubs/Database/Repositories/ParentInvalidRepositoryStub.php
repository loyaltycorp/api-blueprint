<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories;

use LoyaltyCorp\ApiBlueprint\Database\Repositories\ResourceRepository;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ResourceModelStub;

class ParentInvalidRepositoryStub extends ResourceRepository
{
    /**
     * Get parent class managed by the repository.
     * It can be null when dealing with root resources.
     *
     * @return null|string
     */
    protected function getParentClass(): ?string
    {
        return 'invalid';
    }

    /**
     * Get resource class managed by the repository.
     *
     * @return string
     */
    protected function getResourceClass(): string
    {
        return ResourceModelStub::class;
    }
}
