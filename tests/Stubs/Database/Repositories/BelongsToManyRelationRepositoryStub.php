<?php
declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories;

use LoyaltyCorp\ApiBlueprint\Database\Repositories\ResourceRepository;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ChildBelongsToManyModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ParentBelongsToManyModelStub;

class BelongsToManyRelationRepositoryStub extends ResourceRepository
{
    /**
     * Get resource class managed by the repository.
     *
     * @return string
     */
    protected function getResourceClass(): string
    {
        return ChildBelongsToManyModelStub::class;
    }

    protected function getParentClass(): ?string
    {
        return ParentBelongsToManyModelStub::class;
    }

    protected function getParentRelation(): ?string
    {
        return 'children';
    }
}
