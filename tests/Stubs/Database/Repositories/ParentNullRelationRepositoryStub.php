<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories;

class ParentNullRelationRepositoryStub extends ParentRelationRepositoryStub
{
    /**
     * Get relation name on parent.
     * It can be null when dealing with root resources.
     *
     * @return null|string
     */
    protected function getParentRelation(): ?string
    {
        parent::getParentRelation();

        return null;
    }
}
