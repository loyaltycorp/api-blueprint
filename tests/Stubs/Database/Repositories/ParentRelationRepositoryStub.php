<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories;

use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ChildModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ParentModelStub;

class ParentRelationRepositoryStub extends ResourceRepositoryStub
{
    /**
     * Get resource class managed by the repository.
     *
     * @return string
     */
    protected function getResourceClass(): string
    {
        return ChildModelStub::class;
    }

    /**
     * Get parent class managed by the repository.
     * It can be null when dealing with root resources.
     *
     * @return null|string
     */
    protected function getParentClass(): ?string
    {
        parent::getParentClass();

        return ParentModelStub::class;
    }

    /**
     * Get relation name on parent.
     * It can be null when dealing with root resources.
     *
     * @return null|string
     */
    protected function getParentRelation(): ?string
    {
        parent::getParentRelation();

        return 'children';
    }
}
