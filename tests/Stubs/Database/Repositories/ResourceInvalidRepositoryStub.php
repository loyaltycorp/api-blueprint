<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories;

use LoyaltyCorp\ApiBlueprint\Database\Repositories\ResourceRepository;

class ResourceInvalidRepositoryStub extends ResourceRepository
{
    /**
     * Get resource class managed by the repository.
     *
     * @return string
     */
    protected function getResourceClass(): string
    {
        return 'invalid';
    }
}
