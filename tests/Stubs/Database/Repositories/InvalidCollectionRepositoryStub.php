<?php
declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories;

use LoyaltyCorp\ApiBlueprint\Database\Repositories\ResourceRepository;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\InvalidCollectionStub;

class InvalidCollectionRepositoryStub extends ResourceRepository
{
    /**
     * Get resource class managed by the repository.
     *
     * @return string
     */
    protected function getResourceClass(): string
    {
        return InvalidCollectionStub::class;
    }
}
