<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Console;

use Laravel\Lumen\Console\Kernel;
use LoyaltyCorp\ApiBlueprint\Commands\GenerateAdminKey;

class ConsoleKernelStub extends Kernel
{
    /**
     * @var array
     */
    protected $commands = [
          GenerateAdminKey::class
    ];
}
