<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Controllers;

use LoyaltyCorp\ApiBlueprint\Controllers\LumenResourceController;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\ResourceRepositoryStub;

class ResourceControllerStub extends LumenResourceController
{
    /**
     * Defines repository used by the current controller.
     *
     * @return string
     */
    protected function getRepositoryClass(): string
    {
        return ResourceRepositoryStub::class;
    }
}
