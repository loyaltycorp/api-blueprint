<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Controllers;

use LoyaltyCorp\ApiBlueprint\Controllers\LumenResourceController;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\DeleteExceptionRepositoryStub;

class DeleteExceptionControllerStub extends LumenResourceController
{
    /**
     * Defines repository used by the current controller.
     *
     * @return string
     */
    protected function getRepositoryClass(): string
    {
        return DeleteExceptionRepositoryStub::class;
    }
}
