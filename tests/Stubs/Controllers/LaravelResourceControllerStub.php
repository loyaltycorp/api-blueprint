<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Controllers;

use LoyaltyCorp\ApiBlueprint\Controllers\LaravelResourceController;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Repositories\ResourceRepositoryStub;

class LaravelResourceControllerStub extends LaravelResourceController
{
    /**
     * Defines repository used by the current controller.
     *
     * @return string
     */
    protected function getRepositoryClass(): string
    {
        return ResourceRepositoryStub::class;
    }
}
