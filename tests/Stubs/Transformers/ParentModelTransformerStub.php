<?php declare(strict_types = 1);

namespace Tests\LoyaltyCorp\ApiBlueprint\Stubs\Transformers;

use Illuminate\Support\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\NullResource;
use LoyaltyCorp\ApiBlueprint\Transformers\ResourceTransformer;
use League\Fractal\Resource\Collection as FractalCollection;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ParentModelStub;
use Tests\LoyaltyCorp\ApiBlueprint\Stubs\Database\Models\ResourceModelStub;

class ParentModelTransformerStub extends ResourceTransformer
{
    protected $availableIncludes = ['testModel', 'testModelNull', 'testModelCollection'];

    /**
     * @param ParentModelStub $parentModelStub
     *
     * @return Item|NullResource
     */
    public function includeTestModel(ParentModelStub $parentModelStub)
    {
        return $this->includeResource($parentModelStub->testModel);
    }

    /**
     * Include a null resource.
     *
     * @param TestParentModelStub $parentModelStub
     *
     * @return Item|NullResource
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) Parameter not used for test purpose but still have to define it
     */
    public function includeTestModelNull(ParentModelStub $parentModelStub)
    {
        return $this->includeResource(null);
    }

    /**
     * Include a collection resource.
     *
     * @param TestParentModelStub $parentModelStub
     *
     * @return FractalCollection
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) Parameter not used for test purpose but still have to define it
     */
    public function includeTestModelCollection(ParentModelStub $parentModelStub): FractalCollection
    {
        $resource = new ResourceModelStub();

        return $this->includeCollection($resource, new Collection());
    }
}
