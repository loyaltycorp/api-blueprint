<?php declare(strict_types = 1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Avoid warning for laravel code
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class CreateChildBelongsToManyModelStubParentBelongsToManyModelStubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_belongs_to_many_model_stub_parent_belongs_to_many_model_stub', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parent_belongs_to_many_model_stub_id');
            $table->string('child_belongs_to_many_model_stub_id');
            $table->string('extra')->nullable();
            $table->timestamps();

            $table
                ->foreign('parent_belongs_to_many_model_stub_id')
                ->references('id')
                ->on('parent_belongs_to_many_model_stubs')
                ->onDelete('cascade');
            $table
                ->foreign('child_belongs_to_many_model_stub_id')
                ->references('id')
                ->on('child_belongs_to_many_model_stubs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_belongs_to_many_model_stub_parent_belongs_to_many_model_stub');
    }
}
