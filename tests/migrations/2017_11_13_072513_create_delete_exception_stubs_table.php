<?php declare(strict_types = 1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Avoid warning for laravel code
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class CreateDeleteExceptionStubsTable extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delete_exception_stubs');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delete_exception_stubs', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('fillable1');
            $table->string('fillable2')->nullable();
            $table->string('fillable3')->nullable();
            $table->timestamps();
        });
    }
}
