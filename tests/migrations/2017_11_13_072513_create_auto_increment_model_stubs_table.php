<?php declare(strict_types = 1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Avoid warning for laravel code
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class CreateAutoIncrementModelStubsTable extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_increment_model_stubs');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_increment_model_stubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fillable1');
            $table->string('fillable2')->nullable();
            $table->string('fillable3')->nullable();
            $table->timestamps();
        });
    }
}
