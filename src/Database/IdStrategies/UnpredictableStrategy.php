<?php
declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Database\IdStrategies;

use Closure;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceIdStrategyInterface;

class UnpredictableStrategy implements ResourceIdStrategyInterface
{
    /**
     * Return closure use to set the ID on the current resource.
     *
     * @return Closure
     */
    public function getCreatingClosure(): Closure
    {
        return function (ResourceModel $model) {
            $model->id = \uniqid('', false);
        };
    }

    /**
     * Determine if the strategy is using auto increment.
     *
     * @return bool
     */
    public function isIncrementing(): bool
    {
        return false;
    }
}
