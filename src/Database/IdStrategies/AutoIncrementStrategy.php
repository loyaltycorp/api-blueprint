<?php
declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Database\IdStrategies;

use Closure;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceIdStrategyInterface;

class AutoIncrementStrategy implements ResourceIdStrategyInterface
{
    /**
     * Return closure use to set the ID on the current resource.
     *
     * @return Closure
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function getCreatingClosure(): Closure
    {
        return function (ResourceModel $model) {
            // This strategy does not perform any action on $model.
        };
    }

    /**
     * Determine if the strategy is using auto increment.
     *
     * @return bool
     */
    public function isIncrementing(): bool
    {
        return true;
    }
}
