<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Database\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use InvalidArgumentException;
use LoyaltyCorp\ApiBlueprint\Database\Collections\ResourceCollection;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;
use LoyaltyCorp\ApiBlueprint\Exceptions\BaseException;
use LoyaltyCorp\ApiBlueprint\Exceptions\InvalidRelationException;
use LoyaltyCorp\ApiBlueprint\Exceptions\InvalidResourceException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceNotFoundException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ValidationFailedException;
use LoyaltyCorp\ApiBlueprint\Helpers\Interfaces\PaginatedResponseBuilderInterface;
use LoyaltyCorp\ApiBlueprint\Helpers\Interfaces\QueryModifierInterface;
use LoyaltyCorp\ApiBlueprint\Helpers\PaginatedResponseBuilder;
use LoyaltyCorp\ApiBlueprint\Helpers\QueryModifier;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceRepositoryInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
abstract class ResourceRepository implements ResourceRepositoryInterface
{
    /**
     * @var null|string|int
     */
    protected $currentPrimaryKey;

    /**
     * @var null|string|int
     */
    protected $parentPrimaryKey;

    /**
     * @var null|ResourceModel
     */
    private $model;

    /**
     * @var null|ResourceModel
     */
    private $parent;

    /**
     * @var null|Relation
     */
    private $parentRelation;

    /**
     * @var QueryModifierInterface
     */
    private $queryModifier;

    /**
     * Get collection of the resource managed by the repository.
     *
     * @param array|null  $filters
     * @param string|null $orderBy
     * @param string|null $search
     *
     * @return ResourceCollection
     *
     * @throws ResourceNotFoundException
     * @throws InvalidResourceException
     * @throws InvalidRelationException
     */
    public function all(array $filters = null, string $orderBy = null, string $search = null): ResourceCollection
    {
        $all = $this->modifyQuery($filters, $orderBy, $search)->get();

        if ($all instanceof ResourceCollection) {
            return $all;
        }

        throw new InvalidResourceException(\sprintf(
            'Return has to be an instance of %s, %s given',
            ResourceCollection::class,
            \is_object($all) ? \get_class($all) : \gettype($all)
        ));
    }

    /**
     * Delete the given model from the database.
     *
     * @param ResourceModel $model
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function delete(ResourceModel $model): bool
    {
        $query = $this->getQuery();

        // If the relationship is BelongsToMany we detach the model to it
        if ($query instanceof BelongsToMany) {
            return $this->handleBelongsToManyDelete($query, $model);
        }

        return $model->delete() ?? false;
    }

    /**
     * Instantiate resource model and fill it using given attributes.
     *
     * @param array       $attributes
     * @param null|string $validation Name of validation rules set to use to validate attributes
     *
     * @return ResourceModel
     *
     * @throws ResourceErrorException
     * @throws ResourceNotFoundException
     * @throws InvalidResourceException  If class is null
     *                                   If class does not exist
     * @throws ValidationFailedException
     * @throws BaseException             If model does not define validation rules properly
     */
    public function fill(array $attributes, string $validation = null): ResourceModel
    {
        try {
            $model = null !== $this->currentPrimaryKey ? $this->get() : $this->model();
            $model->fill($attributes);
        } catch (BaseException $exception) {
            throw new ResourceErrorException($exception->getExtendedMessage());
        } catch (MassAssignmentException $exception) {
            throw new ResourceErrorException(\sprintf(
                'Mass assignment error on %s::$%s. Please verify the attributes configuration for this model.',
                $this->getResourceClass(),
                $exception->getMessage()
            ));
        }

        $model->validate($validation ?? 'default');

        return $model;
    }

    /**
     * Get the current record of the resource managed by the repository.
     *
     * @return ResourceModel
     *
     * @throws ResourceNotFoundException
     * @throws InvalidResourceException If class is null
     *                                  If class does not exist
     * @throws InvalidRelationException
     */
    public function get(): ResourceModel
    {
        try {
            return $this->getQuery()->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            throw new ResourceNotFoundException($exception->getMessage());
        }
    }

    /**
     * Get the current primary key of the resource managed by the repository.
     *
     * @return int|string|null
     */
    public function getCurrentPrimaryKey()
    {
        return $this->currentPrimaryKey;
    }

    /**
     * Get the current parent record of the resource managed by the repository.
     *
     * @return ResourceModel
     *
     * @throws ResourceNotFoundException
     * @throws InvalidResourceException If class is null
     *                                  If class does not exist
     */
    public function getParent(): ResourceModel
    {
        if (null !== $this->parent) {
            return $this->parent;
        }

        $parent = $this->instantiateParent($this->getParentClass());

        try {
            $this->parent = $parent->where($parent->getResourcePrimaryKey(), $this->parentPrimaryKey)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            throw new ResourceNotFoundException($exception->getMessage());
        }

        return $this->parent;
    }

    /**
     * Get the current parent primary key of the resource managed by the repository.
     *
     * @return int|string|null
     */
    public function getParentPrimaryKey()
    {
        return $this->parentPrimaryKey;
    }

    /**
     * Get the current record of the resource managed by the repository but doesn't use the parent
     * relationship to do it even if parent is present.
     *
     * @return ResourceModel
     *
     * @throws \LoyaltyCorp\ApiBlueprint\Exceptions\InvalidResourceException
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getWithoutParent(): ResourceModel
    {
        return $this->getBuilderWithWhereOnPrimaryKey()->firstOrFail();
    }

    /**
     * Get paginated collection.
     *
     * @param array|null  $filters
     * @param string|null $orderBy
     * @param string|null $search
     *
     * @return PaginatedResponseBuilderInterface
     *
     * @throws ResourceNotFoundException
     * @throws InvalidResourceException
     * @throws InvalidRelationException
     * @throws ResourceErrorException If pagination went wrong
     */
    public function index(
        array $filters = null,
        string $orderBy = null,
        string $search = null
    ): PaginatedResponseBuilderInterface {
        try {
            return new PaginatedResponseBuilder(
                $this->modifyQuery($filters, $orderBy, $search)->paginate(),
                $this->model()->getTransformer(),
                $this->model()->getResourceCollectionKey()
            );
        } catch (InvalidArgumentException $exception) {
            throw new ResourceErrorException($exception->getMessage());
        }
    }

    /**
     * Save the given model in the database.
     *
     * @param ResourceModel $model
     * @param null|array    $options
     *
     * @return bool
     *
     * @throws \LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException
     * @throws \LoyaltyCorp\ApiBlueprint\Exceptions\ResourceNotFoundException
     * @throws \LoyaltyCorp\ApiBlueprint\Exceptions\InvalidResourceException
     * @throws \LoyaltyCorp\ApiBlueprint\Exceptions\InvalidRelationException
     */
    public function save(ResourceModel $model, array $options = null): bool
    {
        $query = $this->getQuery();

        // If the model doesn't belong to any relationship just use its save function
        if ($query instanceof ResourceModel || $query instanceof Builder) {
            return $model->save($options ?? []);
        }

        // If the relationship is BelongsToMany we attach the model to it
        if ($query instanceof BelongsToMany) {
            return $this->handleBelongsToManySave($query, $model, $options);
        }

        // Otherwise just use relationship's save function
        $query->save($model);

        return true;
    }

    /**
     * Set primary key of the record of the resource managed by the repository.
     *
     * @param int|string|null $currentPrimaryKey
     *
     * @return ResourceRepositoryInterface
     */
    public function setCurrentPrimaryKey($currentPrimaryKey = null): ResourceRepositoryInterface
    {
        $this->currentPrimaryKey = $currentPrimaryKey;
        $this->model = null;

        return $this;
    }

    /**
     * Set primary key of the parent record of the resource managed by the repository.
     *
     * @param int|string|null $parentPrimaryKey
     *
     * @return ResourceRepositoryInterface
     */
    public function setParentPrimaryKey($parentPrimaryKey = null): ResourceRepositoryInterface
    {
        $this->parentPrimaryKey = $parentPrimaryKey;
        $this->parent = null;
        $this->parentRelation = null;

        return $this;
    }

    /**
     * Set query modifier to perform modifications before pagination.
     *
     * @param QueryModifierInterface $queryModifier
     *
     * @return ResourceRepositoryInterface
     */
    public function setQueryModifier(QueryModifierInterface $queryModifier): ResourceRepositoryInterface
    {
        $this->queryModifier = $queryModifier;

        return $this;
    }

    /**
     * Get resource class managed by the repository.
     *
     * @return string
     */
    abstract protected function getResourceClass(): string;

    /**
     * Get parent class managed by the repository.
     * It can be null when dealing with root resources.
     *
     * @return null|string
     */
    protected function getParentClass(): ?string
    {
        return null;
    }

    /**
     * Get relation name on parent.
     * It can be null when dealing with root resources.
     *
     * @return null|string
     */
    protected function getParentRelation(): ?string
    {
        return null;
    }

    /**
     * Get query builder with where condition on current primary key.
     *
     * @return ResourceModel|Builder
     *
     * @throws \LoyaltyCorp\ApiBlueprint\Exceptions\InvalidResourceException
     */
    private function getBuilderWithWhereOnPrimaryKey()
    {
        $model = $this->model();

        return $model->where($model->getResourcePrimaryKey(), $this->currentPrimaryKey);
    }

    /**
     * Get primary key with table name as prefix.
     *
     * @return string
     *
     * @throws InvalidResourceException
     */
    private function getPrimaryKeyWithTable(): string
    {
        return \sprintf('%s.%s', $this->model()->getTable(), $this->model()->getResourcePrimaryKey());
    }

    /**
     * Instantiate the query builder.
     *
     * @return Relation|ResourceModel|Builder
     *
     * @throws InvalidResourceException
     * @throws InvalidRelationException
     * @throws ResourceNotFoundException
     */
    private function getQuery()
    {
        if (null !== $this->parentPrimaryKey) {
            return $this->parentRelation();
        }

        if (null !== $this->currentPrimaryKey) {
            return $this->getBuilderWithWhereOnPrimaryKey();
        }

        return $this->model();
    }

    /**
     * @return QueryModifierInterface
     *
     * @throws InvalidResourceException
     */
    private function getQueryModifier(): QueryModifierInterface
    {
        if (null !== $this->queryModifier) {
            return $this->queryModifier;
        }

        return new QueryModifier(
            $this->model()->getFilterFields(),
            $this->model()->getOrderByAndDirection(),
            $this->model()->getSearchFields()
        );
    }

    /**
     * Detach model from parent.
     *
     * @param BelongsToMany $relation
     * @param ResourceModel $model
     *
     * @return bool
     */
    private function handleBelongsToManyDelete(BelongsToMany $relation, ResourceModel $model): bool
    {
        $relation->detach($model);

        return true;
    }

    /**
     * Attach model to parent if it's not already attached.
     *
     * @param BelongsToMany $relation
     * @param ResourceModel $model
     * @param null|array    $options
     *
     * @return bool
     *
     * @throws \LoyaltyCorp\ApiBlueprint\Exceptions\ResourceNotFoundException
     * @throws \LoyaltyCorp\ApiBlueprint\Exceptions\InvalidResourceException
     */
    private function handleBelongsToManySave(BelongsToMany $relation, ResourceModel $model, array $options = null): bool
    {
        $relationName = $this->getParentRelation();

        if (!$this->getParent()->$relationName->contains($model)) {
            $relation->attach($model, $options ?? []);
        }

        return true;
    }

    /**
     * Instantiate parent model from class.
     *
     * @param string|null $class
     *
     * @return ResourceModel
     *
     * @throws InvalidResourceException If class is null
     *                                  If class does not exist
     */
    private function instantiateParent(string $class = null): ResourceModel
    {
        if (null === $class) {
            throw new InvalidResourceException(\sprintf(
                'Repository %s is used in a child model context but does not define any parent class.',
                \get_class($this)
            ));
        }

        if (!\class_exists($class)) {
            throw new InvalidResourceException(\sprintf(
                'Repository %s defines parent class "%s" but it does not exist.',
                \get_class($this),
                $class
            ));
        }

        return new $class();
    }

    /**
     * Instantiate resource model from class.
     *
     * @param string|null $class
     *
     * @return ResourceModel
     *
     * @throws InvalidResourceException If class does not exist
     */
    private function instantiateResource(string $class): ResourceModel
    {
        if (!\class_exists($class)) {
            throw new InvalidResourceException(\sprintf(
                'Repository %s defines resource class "%s" but it does not exist.',
                \get_class($this),
                $class
            ));
        }

        return new $class();
    }

    /**
     * Get resource model instance.
     *
     * @return ResourceModel
     *
     * @throws InvalidResourceException If class is null
     *                                  If class does not exist
     */
    private function model(): ResourceModel
    {
        if (null !== $this->model) {
            return $this->model;
        }

        $this->model = $this->instantiateResource($this->getResourceClass());

        return $this->model;
    }

    /**
     * @param array|null  $filters
     * @param string|null $orderBy
     * @param string|null $search
     *
     * @return Relation|ResourceModel
     *
     * @throws ResourceNotFoundException
     * @throws InvalidResourceException
     * @throws InvalidRelationException
     */
    private function modifyQuery(array $filters = null, string $orderBy = null, string $search = null)
    {
        $query = $this->getQuery();
        $queryModifier = $this->getQueryModifier();

        $queryModifier
            ->applyFilters($query, $filters ?? [])
            ->applyOrderBy($query, $orderBy)
            ->applySearch($query, $search);

        return $query;
    }

    /**
     * Get relation from resource parent model.
     *
     * @return Relation
     *
     * @throws ResourceNotFoundException If parent record not found
     * @throws InvalidResourceException  If class is null
     *                                   If class does not exist
     * @throws InvalidRelationException
     */
    private function parentRelation(): Relation
    {
        if (null !== $this->parentRelation) {
            return $this->parentRelation;
        }

        // Function name on parent object
        $relation = $this->getParentRelation();
        $this->validateParentRelation($this->getParent(), $relation);

        $this->parentRelation = $this->getParent()->$relation();

        if (null !== $this->currentPrimaryKey) {
            $this->parentRelation->where($this->getPrimaryKeyWithTable(), $this->currentPrimaryKey);
        }

        return $this->parentRelation;
    }

    /**
     * @param ResourceModel $parent
     * @param string|null   $relation
     *
     * @throws InvalidRelationException
     */
    private function validateParentRelation(ResourceModel $parent, string $relation = null)
    {
        if (null === $relation) {
            throw new InvalidRelationException(\sprintf(
                'Repository %s defines parent class "%s" but does not define any relation for it.',
                \get_class($this),
                \get_class($parent)
            ));
        }

        if (!\method_exists($parent, $relation)) {
            throw new InvalidRelationException(\sprintf(
                'Repository %s defines parent relation as "%s::%s()" but this function does not exist.',
                \get_class($this),
                \get_class($parent),
                $relation
            ));
        }
    }
}
