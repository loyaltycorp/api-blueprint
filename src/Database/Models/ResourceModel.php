<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Database\Models;

use Illuminate\Database\QueryException;
use LoyaltyCorp\ApiBlueprint\Database\Collections\ResourceCollection;
use LoyaltyCorp\ApiBlueprint\Database\Models\Traits\DealsWithApplies;
use LoyaltyCorp\ApiBlueprint\Database\Models\Traits\HasChildren;
use LoyaltyCorp\ApiBlueprint\Database\Models\Traits\HasResourceFunctions;
use LoyaltyCorp\ApiBlueprint\Database\Models\Traits\HasValidation;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException;
use LoyaltyCorp\ApiBlueprint\External\Libraries\Model as BaseModel;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceInterface;

abstract class ResourceModel extends BaseModel implements ResourceInterface
{
    use DealsWithApplies;
    use HasChildren;
    use HasResourceFunctions;
    use HasValidation;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        // Disable id auto increment.
        $this->incrementing = $this->getResourceIdStrategy()->isIncrementing();

        parent::__construct($attributes);
    }

    /**
     * Make sure that all collections from database are instance of ResourceCollection.
     *
     * @param array $models
     *
     * @return ResourceCollection
     */
    public function newCollection(array $models = []): ResourceCollection
    {
        return new ResourceCollection($models);
    }

    /**
     * Save resource.
     *
     * @param array $options
     *
     * @return bool
     *
     * @throws ResourceErrorException
     */
    public function save(array $options = []): bool
    {
        $success = false;
        $maxAttempts = 10;

        while ($success === false) {
            try {
                $success = parent::save($options);
            } catch (QueryException $exception) {
                //Keep looping if the exception is for duplicate id to generate new unique id
                if ($maxAttempts > 0
                    && \property_exists($exception, 'errorInfo')
                    && \in_array($exception->errorInfo[1], [1062, 19], true)) {
                    $maxAttempts--;
                    continue;
                }
                throw new ResourceErrorException($exception->getMessage());
            }
        }

        return $success;
    }

    /**
     * Call parent only but make model compatible with ResourceInterface.
     *
     * @return array
     */
    public function toArray(): array
    {
        return parent::toArray();
    }

    /**
     * Register event listener to generate resource id on creating.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating((new static())->getResourceIdStrategy()->getCreatingClosure());
    }
}
