<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\Database\Models\Traits;

trait DealsWithApplies
{
    /**
     * Get resource fields available for filtering.
     *
     * @return array
     */
    public function getFilterFields(): array
    {
        return \array_merge($this->getFillable(), ['id']);
    }

    /**
     * Get field to order by and direction.
     * Format: <field>,<direction>
     *
     * @return string
     */
    public function getOrderByAndDirection(): string
    {
        return 'id,asc';
    }

    /**
     * Get resource fields used to perform search on it.
     *
     * @return array
     */
    public function getSearchFields(): array
    {
        return $this->getFillable();
    }
}
