<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Database\Models\Traits;

use LoyaltyCorp\ApiBlueprint\Database\IdStrategies\UnpredictableStrategy;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\InflectorInterface;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\StrInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceIdStrategyInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface;
use LoyaltyCorp\ApiBlueprint\Transformers\ResourceTransformer;
use ReflectionClass;
use ReflectionException;

trait HasResourceFunctions
{
    /**
     * Get resource key for a collection of items.
     *
     * @return string
     *
     * @throws ResourceErrorException If cannot get resource name
     */
    public function getResourceCollectionKey(): string
    {
        return \container(InflectorInterface::class)->pluralize($this->getResourceName());
    }

    /**
     * Get resource foreign key.
     * Used to handle relationships.
     *
     * @return string
     */
    public function getResourceForeignKey(): string
    {
        return $this->getForeignKey();
    }

    /**
     * Get resource ID strategy.
     *
     * @return ResourceIdStrategyInterface
     */
    public function getResourceIdStrategy(): ResourceIdStrategyInterface
    {
        return new UnpredictableStrategy();
    }

    /**
     * Get resource key for a single item.
     *
     * @return string
     *
     * @throws ResourceErrorException If cannot get resource name
     */
    public function getResourceItemKey(): string
    {
        return $this->getResourceName();
    }

    /**
     * Get resource limit.
     * Used to limit number of resources per page.
     *
     * @return int
     */
    public function getResourceLimit(): int
    {
        return $this->perPage;
    }

    /**
     * Get resource primary key.
     * Used to retrieve model in database.
     *
     * @return string
     */
    public function getResourcePrimaryKey(): string
    {
        return $this->primaryKey;
    }

    /**
     * Get transformer.
     *
     * @return ResourceTransformerInterface
     */
    public function getTransformer(): ResourceTransformerInterface
    {
        return new ResourceTransformer();
    }

    /**
     * Get short name of the class.
     *
     * @return string
     *
     * @throws ResourceErrorException
     */
    private function getResourceName(): string
    {
        try {
            return \container(StrInterface::class)->lower((new ReflectionClass($this))->getShortName());
        } catch (ReflectionException $exception) {
            throw new ResourceErrorException($exception->getMessage());
        }
    }
}
