<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\Database\Models\Traits;

use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceInterface;

trait HasChildren
{
    /**
     * Add child to model.
     *
     * @param ResourceInterface $resource
     * @param string|null       $foreignKey Foreign key used for relationship
     *
     * @return mixed
     */
    public function addChild(ResourceInterface $resource, string $foreignKey = null): ResourceInterface
    {
        if (null === $foreignKey) {
            $foreignKey = $this->getResourceForeignKey();
        }

        $resource->{$foreignKey} = $this->id;

        return $this;
    }

    /**
     * Add children to model.
     *
     * @param array       $resources
     * @param string|null $foreignKey
     *
     * @return mixed
     */
    public function addChildren(array $resources, string $foreignKey = null): ResourceInterface
    {
        foreach ($resources as $resource) {
            $this->addChild($resource, $foreignKey);
        }

        return $this;
    }
}
