<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\Database\Models\Traits;

use LoyaltyCorp\ApiBlueprint\Exceptions\BaseException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ValidationFailedException;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\ValidatorInterface;
use LoyaltyCorp\ApiBlueprint\External\Libraries\Validator;

trait HasValidation
{
    /**
     * Return array of validation rules sets following Laravel Validation rules format.
     *
     * @return array
     */
    abstract protected function getValidationRules(): array;

    /**
     * @return ValidatorInterface
     */
    protected function getValidator(): ValidatorInterface
    {
        return new Validator();
    }

    /**
     * Get validate model based on validation set.
     *
     * @param string $set
     *
     * @return bool
     *
     * @throws BaseException If no rules property defined
     *                       If rules are not an array
     * @throws ValidationFailedException
     */
    public function validate(string $set): bool
    {
        $validationRules = $this->getValidationRules();

        $rules = null;
        foreach ([$set, 'default'] as $rulesSet) {
            if (!empty($validationRules[$rulesSet])) {
                $rules = $validationRules[$rulesSet];
                break;
            }
        }

        if (null === $rules) {
            throw new BaseException(\sprintf(
                'Rules set %s does not exist and no default rules set is defined.',
                $set
            ));
        }

        $validator = $this->getValidator()->make($this->attributesToArray(), $rules);

        if ($validator->fails()) {
            throw new ValidationFailedException(\implode(', ', $validator->errors()->all()));
        }

        return true;
    }
}
