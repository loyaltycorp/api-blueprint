<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Controllers\Traits;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use LoyaltyCorp\ApiBlueprint\Exceptions\BaseException;
use LoyaltyCorp\ApiBlueprint\Exceptions\InvalidRelationException;
use LoyaltyCorp\ApiBlueprint\Exceptions\InvalidResourceException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceNotFoundException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ValidationFailedException;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\FractalInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceControllerInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceRepositoryInterface;

trait ResourceControllerTrait
{
    /** @var FractalInterface */
    protected $fractal;

    /** @var Request */
    protected $request;

    /** @var ResourceRepositoryInterface */
    protected $repository;

    /**
     * Destroy a single resource.
     *
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     *
     * @throws ResourceNotFoundException
     * @throws ResourceErrorException    If cannot get resource item key
     *                                   If cannot delete resource
     * @throws InvalidRelationException  If relation on parent is invalid
     * @throws InvalidResourceException  If resource is invalid
     */
    public function destroy($primaryKey, $parentKey = null): JsonResponse
    {
        $current = $this->getRepository($primaryKey, $parentKey)->get();

        $this->beforeDeleting($current, $parentKey);

        try {
            $this->getRepository()->delete($current);
        } catch (Exception $exception) {
            throw new ResourceErrorException($exception->getMessage());
        }

        $this->afterDeleting($current, $parentKey);

        return $this->emptyResponse();
    }

    /**
     * Get paginated list of resources.
     *
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     *
     * @throws ResourceNotFoundException
     * @throws ResourceErrorException    If cannot get resource item key
     * @throws InvalidRelationException  If relation on parent is invalid
     * @throws InvalidResourceException  If resource is invalid
     */
    public function index($parentKey = null): JsonResponse
    {
        $this->fractal->parseIncludesAndExcludes(
            $this->request->get('includes', []),
            $this->request->get('excludes', [])
        );

        $responseBuilder = $this->getRepository(null, $parentKey)->index();

        return $this->paginateResponse(
            $responseBuilder->getPaginator(),
            $responseBuilder->getTransformer(),
            $responseBuilder->getResourceKey()
        );
    }

    /**
     * Get a single resource.
     *
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     *
     * @throws ResourceNotFoundException
     * @throws ResourceErrorException    If cannot get resource item key
     * @throws InvalidRelationException  If relation on parent is invalid
     * @throws InvalidResourceException  If resource is invalid
     */
    public function show($primaryKey, $parentKey = null): JsonResponse
    {
        $this->fractal->parseIncludesAndExcludes(
            $this->request->get('includes', []),
            $this->request->get('excludes', [])
        );

        return $this->resourceResponse($this->getRepository($primaryKey, $parentKey)->get());
    }

    /**
     * Create and store a single resource.
     *
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     *
     * @throws ResourceErrorException    If resource instantiation went wrong
     * @throws BaseException             If resource does not define validation rules properly
     * @throws ValidationFailedException If validation failed
     * @throws InvalidResourceException  If parent resource is invalid
     * @throws ResourceNotFoundException If parent resource is not found
     */
    public function store($parentKey = null): JsonResponse
    {
        $new = $this
            ->getRepository(null, $parentKey)
            ->fill($this->request->all(), ResourceControllerInterface::ACTION_STORE);

        $this->beforeSaving($new, ResourceControllerInterface::ACTION_STORE, $parentKey);

        $this->getRepository()->save($new);

        $this->afterSaving($new, ResourceControllerInterface::ACTION_STORE, $parentKey);

        $this->fractal->parseIncludesAndExcludes(
            $this->request->get('includes', []),
            $this->request->get('excludes', [])
        );

        return $this->resourceResponse($new);
    }

    /**
     * Update a single resource.
     *
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     *
     * @throws ResourceNotFoundException
     * @throws InvalidResourceException
     * @throws ResourceErrorException    If resource instantiation went wrong
     * @throws BaseException             If resource does not define validation rules properly
     * @throws ValidationFailedException If validation failed
     */
    public function update($primaryKey, $parentKey = null): JsonResponse
    {
        $current = $this
            ->getRepository($primaryKey, $parentKey)
            ->fill($this->request->all(), ResourceControllerInterface::ACTION_UPDATE);

        $this->beforeSaving($current, ResourceControllerInterface::ACTION_UPDATE, $parentKey);

        $this->getRepository()->save($current);

        $this->afterSaving($current, ResourceControllerInterface::ACTION_UPDATE, $parentKey);

        $this->fractal->parseIncludesAndExcludes(
            $this->request->get('includes', []),
            $this->request->get('excludes', [])
        );

        return $this->resourceResponse($current);
    }

    /**
     * Defines repository used by the current controller.
     *
     * @return string
     */
    abstract protected function getRepositoryClass(): string;

    /**
     * Get repository instance.
     *
     * @param string|null $primaryKey
     * @param string|null $parentKey
     *
     * @return ResourceRepositoryInterface
     */
    final protected function getRepository(string $primaryKey = null, string $parentKey = null): ResourceRepositoryInterface
    {
        if (null !== $this->repository) {
            return $this->repository;
        }

        $class = $this->getRepositoryClass();
        /** @var ResourceRepositoryInterface $repository */
        $repository = new $class();

        $this->repository = $repository
            ->setCurrentPrimaryKey($primaryKey)
            ->setParentPrimaryKey($parentKey);

        return $this->repository;
    }
}
