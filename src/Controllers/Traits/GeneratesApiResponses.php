<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\Controllers\Traits;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\ResourceAbstract;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\FractalInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface;

trait GeneratesApiResponses
{
    /** @var FractalInterface */
    protected $fractal;

    /**
     * Return empty json response.
     *
     * @return JsonResponse
     */
    protected function emptyResponse(): JsonResponse
    {
        return \response()->json();
    }

    /**
     * Return json response for paginated resources collection.
     *
     * @param LengthAwarePaginator         $paginator
     * @param ResourceTransformerInterface $transformer
     * @param string                       $resourceKey
     *
     * @return JsonResponse
     */
    protected function paginateResponse(
        LengthAwarePaginator $paginator,
        ResourceTransformerInterface $transformer,
        string $resourceKey
    ): JsonResponse {
        /** @var \Illuminate\Pagination\LengthAwarePaginator $paginator */
        /** @var \League\Fractal\Resource\Collection $collection */
        $collection = $this->fractal->createCollection(
            $paginator->getCollection(),
            $transformer,
            $resourceKey
        );
        $collection->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return $this->response($collection);
    }

    /**
     * Return json response for resource.
     *
     * @param ResourceModel $resource
     *
     * @return JsonResponse
     *
     * @throws ResourceErrorException If cannot get resource item key
     */
    protected function resourceResponse(ResourceModel $resource): JsonResponse
    {
        $item = $this->fractal->createItem(
            $resource,
            $resource->getTransformer(),
            $resource->getResourceItemKey()
        );

        return $this->response($item);
    }

    /**
     * Return json response for fractal resource.
     *
     * @param ResourceAbstract $resourceAbstract
     *
     * @return JsonResponse
     */
    protected function response(ResourceAbstract $resourceAbstract): JsonResponse
    {
        $data = $this->fractal->createData($resourceAbstract)->toArray();

        return \response()->json($data);
    }
}
