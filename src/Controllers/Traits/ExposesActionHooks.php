<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\Controllers\Traits;

use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;

trait ExposesActionHooks
{
    /**
     * Perform actions after resource deleting.
     *
     * @param ResourceModel   $current
     * @param null|int|string $parentKey
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) This function is a callback to perform actions from children
     *                                                controllers so the parameters here are not used
     */
    protected function afterDeleting(ResourceModel $current, $parentKey = null): void
    {
        //Perform actions after resource deleting
    }

    /**
     * Perform actions after resource saving.
     *
     * @param ResourceModel   $current
     * @param string          $action
     * @param null|int|string $parentKey
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) This function is a callback to perform actions from children
     *                                                controllers so the parameters here are not used
     */
    protected function afterSaving(ResourceModel $current, string $action, $parentKey = null): void
    {
        //Perform actions after resource saving
    }
    
    /**
     * Perform actions before resource deleting.
     *
     * @param ResourceModel   $current
     * @param null|int|string $parentKey
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) This function is a callback to perform actions from children
     *                                                controllers so the parameters here are not used
     */
    protected function beforeDeleting(ResourceModel $current, $parentKey = null): void
    {
        //Perform actions before resource deleting
    }

    /**
     * Perform actions before resource saving.
     *
     * @param ResourceModel   $current
     * @param string          $action
     * @param null|int|string $parentKey
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter) This function is a callback to perform actions from children
     *                                                controllers so the parameters here are not used
     */
    protected function beforeSaving(ResourceModel $current, string $action, $parentKey = null): void
    {
        //Perform actions before resource saving
    }
}
