<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;
use LoyaltyCorp\ApiBlueprint\Controllers\Traits\ExposesActionHooks;
use LoyaltyCorp\ApiBlueprint\Controllers\Traits\GeneratesApiResponses;
use LoyaltyCorp\ApiBlueprint\Controllers\Traits\ResourceControllerTrait;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\FractalInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceControllerInterface;

/**
 * @property FractalInterface fractal
 * @property Request          request
 */
abstract class LumenResourceController extends Controller implements ResourceControllerInterface
{
    use ExposesActionHooks;
    use GeneratesApiResponses;
    use ResourceControllerTrait;

    /**
     * @param Request          $request
     * @param FractalInterface $fractal
     */
    public function __construct(Request $request, FractalInterface $fractal)
    {
        $this->request = $request;
        $this->fractal = $fractal;
    }
}
