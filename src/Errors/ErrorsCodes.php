<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Errors;

/**
 * This class is responsible to host constants defining internal errors codes.
 */
class ErrorsCodes
{
    const API_SOMETHING_WRONG = 'AP-SO-WR';

    const API_AUTHENTICATION_UNAUTHORIZED = 'AP-AU-UN';

    const API_AUTHORIZATION_FORBIDDEN = 'AP-AU-FO';

    const API_METHOD_NOT_ALLOWED = 'AP-ME-NO-AL';

    const RESOURCE_VALIDATION_FAILED = 'RE-VA-FA';

    const RESOURCE_NOT_FOUND = 'RE-NO-FO';

    const RESOURCE_ACTION = 'RE-AC';

    const RESOURCE_INVALID = 'RE-IN';

    const RESOURCE_INVALID_RELATION = 'RE-IN-RE';
}
