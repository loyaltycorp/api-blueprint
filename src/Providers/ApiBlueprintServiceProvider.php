<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\Providers;

use Closure;
use Illuminate\Support\ServiceProvider;
use League\Fractal\Serializer\JsonApiSerializer;
use LoyaltyCorp\ApiBlueprint\Commands\GenerateAdminKey;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\FractalInterface;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\InflectorInterface;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\StrInterface;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\ValidatorInterface;
use LoyaltyCorp\ApiBlueprint\External\Libraries\Fractal;
use LoyaltyCorp\ApiBlueprint\External\Libraries\Inflector;
use LoyaltyCorp\ApiBlueprint\External\Libraries\Str;
use LoyaltyCorp\ApiBlueprint\External\Libraries\Validator;
use LoyaltyCorp\ApiBlueprint\Services\Keys\Interfaces\KeyGeneratorInterface;
use LoyaltyCorp\ApiBlueprint\Services\Keys\KeyGenerator;

class ApiBlueprintServiceProvider extends ServiceProvider
{
    /** @var array */
    private $binding = [
        KeyGeneratorInterface::class => KeyGenerator::class,
        InflectorInterface::class => Inflector::class,
        StrInterface::class => Str::class,
        ValidatorInterface::class => Validator::class,
    ];

    /** @var array */
    private $commands = [
        GenerateAdminKey::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        // Simple binding with no configuration
        foreach ($this->binding as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }

        // Commands provided by the package
        $this->commands($this->commands);

        $this->app->singleton(FractalInterface::class, $this->getFractalSingletonClosure());
    }

    /**
     * Function to get the closure to make it testable and improve code coverage.
     *
     * @return Closure
     */
    private function getFractalSingletonClosure(): Closure
    {
        return function () {
            return (new Fractal())->setSerializer(new JsonApiSerializer());
        };
    }
}
