<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions;

use LoyaltyCorp\ApiBlueprint\Errors\ErrorsCodes;

class InvalidResourceException extends BaseException
{
    protected $message = 'Invalid resource';
    protected $code = ErrorsCodes::RESOURCE_INVALID;
    protected $statusCode = 400;
}
