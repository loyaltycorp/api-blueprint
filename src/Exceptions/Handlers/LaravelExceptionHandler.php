<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions\Handlers;

use Illuminate\Foundation\Exceptions\Handler;
use LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\Traits\ExceptionHandlerTrait;

class LaravelExceptionHandler extends Handler
{
    use ExceptionHandlerTrait;
}
