<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions\Handlers;

use Laravel\Lumen\Exceptions\Handler;
use LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\Traits\ExceptionHandlerTrait;

class LumenExceptionHandler extends Handler
{
    use ExceptionHandlerTrait;
}
