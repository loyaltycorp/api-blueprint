<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\Traits;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use LoyaltyCorp\ApiBlueprint\Exceptions\BaseException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceNotFoundException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ValidationFailedException;
use LoyaltyCorp\ApiBlueprint\Interfaces\ExceptionInterface;
use Symfony\Component\HttpFoundation\Response;

trait ExceptionHandlerTrait
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request   $request
     * @param  Exception $exception
     *
     * @return Response
     */
    public function render($request, Exception $exception): Response
    {
        if ($exception instanceof ValidationException) {
            $exception = new ValidationFailedException($this->formatValidationErrors($exception->errors()));
        }

        if ($exception instanceof ModelNotFoundException) {
            $exception = new ResourceNotFoundException($exception->getMessage());
        }

        if (!$exception instanceof ExceptionInterface && !env('APP_DEBUG', false)) {
            $exception = new BaseException();
        }

        if ($exception instanceof ExceptionInterface) {
            return $exception->toResponse();
        }

        return parent::render($request, $exception);
    }

    /**
     * Format validation errors.
     *
     * @param array $errors
     *
     * @return string
     */
    private function formatValidationErrors(array $errors): string
    {
        $messages = [];

        foreach ($errors as $field => $error) {
            $messages[] = \sprintf('%s: %s', $field, $error[0]);
        }

        return \implode(', ', $messages);
    }
}
