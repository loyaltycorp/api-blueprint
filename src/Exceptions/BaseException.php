<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions;

use Illuminate\Http\JsonResponse;
use LoyaltyCorp\ApiBlueprint\Interfaces\ExceptionInterface;

class BaseException extends \Exception implements ExceptionInterface
{
    /** @var string */
    protected $extendedMessage;

    /** @var string */
    protected $message = 'Something went wrong';

    /** @var int */
    protected $statusCode = 500;

    /**
     * @param null|string     $extendedMessage
     * @param null|int|string $code
     * @param null|int        $statusCode
     */
    public function __construct(string $extendedMessage = null, $code = null, int $statusCode = null)
    {
        parent::__construct();

        if (null !== $extendedMessage) {
            $this->extendedMessage = $extendedMessage;
        }
        if (null !== $code) {
            $this->code = $code;
        }
        if (null !== $statusCode) {
            $this->statusCode = $statusCode;
        }
    }

    /**
     * Get extended message, fallback to message if null.
     *
     * @return null|string
     */
    public function getExtendedMessage(): ?string
    {
        return $this->extendedMessage ?? $this->message;
    }

    /**
     * Get array representation of the exception.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'message' => $this->message,
            'extended_message' => $this->extendedMessage,
            'code' => $this->code,
            'status_code' => $this->statusCode
        ];
    }

    /**
     * Get response representation of the exception.
     *
     * @return JsonResponse
     */
    public function toResponse(): JsonResponse
    {
        $message = env('APP_DEBUG') ? $this->extendedMessage ?? $this->message : $this->message;

        return \response()->json([
            'error' => [
                'code' => $this->getCode(),
                'description' => $message
            ]
        ], $this->statusCode);
    }
}
