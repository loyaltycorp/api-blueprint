<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions;

use LoyaltyCorp\ApiBlueprint\Errors\ErrorsCodes;

class InvalidRelationException extends BaseException
{
    protected $message = 'Invalid relation';
    protected $code = ErrorsCodes::RESOURCE_INVALID_RELATION;
    protected $statusCode = 400;
}
