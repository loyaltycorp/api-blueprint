<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions;

use LoyaltyCorp\ApiBlueprint\Errors\ErrorsCodes;

class ResourceNotFoundException extends BaseException
{
    protected $message = 'Resource not found';
    protected $code = ErrorsCodes::RESOURCE_NOT_FOUND;
    protected $statusCode = 404;
}
