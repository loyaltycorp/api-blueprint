<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions;

use LoyaltyCorp\ApiBlueprint\Errors\ErrorsCodes;

class ResourceErrorException extends BaseException
{
    protected $message = 'Resource error';
    protected $code = ErrorsCodes::RESOURCE_ACTION;
    protected $statusCode = 500;
}
