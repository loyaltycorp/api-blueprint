<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions;

use LoyaltyCorp\ApiBlueprint\Errors\ErrorsCodes;

class ForbiddenException extends BaseException
{
    protected $message = 'Forbidden';
    protected $code = ErrorsCodes::API_AUTHORIZATION_FORBIDDEN;
    protected $statusCode = 403;
}
