<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions;

use LoyaltyCorp\ApiBlueprint\Errors\ErrorsCodes;

class UnauthorisedException extends BaseException
{
    protected $message = 'Unauthorised';
    protected $code = ErrorsCodes::API_AUTHENTICATION_UNAUTHORIZED;
    protected $statusCode = 401;
}
