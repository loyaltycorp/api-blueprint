<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Exceptions;

use LoyaltyCorp\ApiBlueprint\Errors\ErrorsCodes;

class ValidationFailedException extends BaseException
{
    protected $message = 'Validation failed';
    protected $code = ErrorsCodes::RESOURCE_VALIDATION_FAILED;
    protected $statusCode = 400;
}
