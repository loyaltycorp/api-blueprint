<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\Services\Keys;

use LoyaltyCorp\ApiBlueprint\Services\Keys\Interfaces\KeyGeneratorInterface;

class KeyGenerator implements KeyGeneratorInterface
{
    /**
     * Generate key.
     *
     * @param string      $content
     * @param null|string $prefix
     *
     * @return string
     */
    public function generate(string $content, string $prefix = null): string
    {
        $prefix = $prefix ?? '';

        return \sprintf('%s%s', $prefix, \md5($content . \time() . \uniqid($prefix, false)));
    }
}
