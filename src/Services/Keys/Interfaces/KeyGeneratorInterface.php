<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Services\Keys\Interfaces;

interface KeyGeneratorInterface
{
    /**
     * Generate key.
     *
     * @param string $content
     * @param null|string $prefix
     *
     * @return string
     */
    public function generate(string $content, string $prefix = null): string;
}
