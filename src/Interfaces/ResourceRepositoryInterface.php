<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Interfaces;

use LoyaltyCorp\ApiBlueprint\Database\Collections\ResourceCollection;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;
use LoyaltyCorp\ApiBlueprint\Exceptions\BaseException;
use LoyaltyCorp\ApiBlueprint\Exceptions\InvalidResourceException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceErrorException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ResourceNotFoundException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ValidationFailedException;
use LoyaltyCorp\ApiBlueprint\Helpers\Interfaces\PaginatedResponseBuilderInterface;
use LoyaltyCorp\ApiBlueprint\Helpers\Interfaces\QueryModifierInterface;

interface ResourceRepositoryInterface
{
    /**
     * Get collection of the resource managed by the repository.
     *
     * @param array|null  $filters
     * @param string|null $orderBy
     * @param string|null $search
     *
     * @return ResourceCollection
     */
    public function all(array $filters = null, string $orderBy = null, string $search = null): ResourceCollection;

    /**
     * Delete the given model from the database.
     *
     * @param ResourceModel $model
     *
     * @return bool
     */
    public function delete(ResourceModel $model): bool;

    /**
     * Instantiate resource model and fill it using given attributes.
     *
     * @param array       $attributes
     * @param null|string $validation Name of validation rules set to use to validate attributes
     *
     * @return ResourceModel
     *
     * @throws ResourceErrorException
     * @throws ResourceNotFoundException
     * @throws InvalidResourceException  If class is null
     *                                   If class does not exist
     * @throws ValidationFailedException
     * @throws BaseException             If model does not define validation rules properly
     */
    public function fill(array $attributes, string $validation = null): ResourceModel;

    /**
     * Get the current record of the resource managed by the repository.
     *
     * @return ResourceModel
     */
    public function get(): ResourceModel;

    /**
     * Get the current primary key of the resource managed by the repository.
     *
     * @return int|string|null
     */
    public function getCurrentPrimaryKey();

    /**
     * Get the current parent record of the resource managed by the repository.
     *
     * @return ResourceModel
     *
     * @throws ResourceNotFoundException
     * @throws InvalidResourceException
     */
    public function getParent(): ResourceModel;

    /**
     * Get the current parent primary key of the resource managed by the repository.
     *
     * @return int|string|null
     */
    public function getParentPrimaryKey();

    /**
     * Get the current record of the resource managed by the repository but doesn't use the parent
     * relationship to do it even if parent is present.
     *
     * @return ResourceModel
     */
    public function getWithoutParent(): ResourceModel;

    /**
     * Get paginated collection of the resource managed by the repository.
     *
     * @param null|array  $filters
     * @param null|string $orderBy
     * @param null|string $search
     *
     * @return PaginatedResponseBuilderInterface
     */
    public function index(
        array $filters = null,
        string $orderBy = null,
        string $search = null
    ): PaginatedResponseBuilderInterface;

    /**
     * Save the given model in the database.
     *
     * @param ResourceModel $model
     * @param array|null    $options
     *
     * @return bool
     */
    public function save(ResourceModel $model, array $options = null): bool;

    /**
     * Set primary key of the record of the resource managed by the repository.
     *
     * @param int|string|null $currentPrimaryKey
     *
     * @return self
     */
    public function setCurrentPrimaryKey($currentPrimaryKey = null): self;

    /**
     * Set primary key of the parent record of the resource managed by the repository.
     *
     * @param int|string|null $parentPrimaryKey
     *
     * @return self
     */
    public function setParentPrimaryKey($parentPrimaryKey = null): self;

    /**
     * Set query modifier to perform modifications before pagination.
     *
     * @param QueryModifierInterface $queryModifier
     *
     * @return ResourceRepositoryInterface
     */
    public function setQueryModifier(QueryModifierInterface $queryModifier): self;
}
