<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Interfaces;

use Illuminate\Http\JsonResponse;

/**
 * Flag for exceptions of the application
 */
interface ExceptionInterface
{
    /**
     * Get array representation of the exception.
     *
     * @return array
     */
    public function toArray(): array;

    /**
     * Get json response representation of the exception.
     *
     * @return JsonResponse
     */
    public function toResponse(): JsonResponse;
}
