<?php
declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Interfaces;

use Closure;

interface ResourceIdStrategyInterface
{
    /**
     * Return closure use to set the ID on the current resource.
     *
     * @return Closure
     */
    public function getCreatingClosure(): Closure;

    /**
     * Determine if the strategy is using auto increment.
     *
     * @return bool
     */
    public function isIncrementing(): bool;
}
