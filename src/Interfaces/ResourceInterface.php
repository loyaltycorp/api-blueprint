<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Interfaces;

use LoyaltyCorp\ApiBlueprint\Exceptions\BaseException;
use LoyaltyCorp\ApiBlueprint\Exceptions\ValidationFailedException;

interface ResourceInterface
{
    /**
     * Add child to model.
     *
     * @param ResourceInterface $resource
     * @param string|null       $foreignKey Foreign key used for relationship
     *
     * @return self
     */
    public function addChild(ResourceInterface $resource, string $foreignKey = null): self;

    /**
     * Add children to model.
     *
     * @param array       $resources
     * @param string|null $foreignKey
     *
     * @return self
     */
    public function addChildren(array $resources, string $foreignKey = null): self;

    /**
     * Get resource fields available for filtering.
     *
     * @return array
     */
    public function getFilterFields(): array;

    /**
     * Get field to order by and direction.
     * Format: <field>,<direction>
     *
     * @return string
     */
    public function getOrderByAndDirection(): string;

    /**
     * Get resource key for a collection of items.
     *
     * @return string
     */
    public function getResourceCollectionKey(): string;

    /**
     * Get resource foreign key.
     * Used to handle relationships.
     *
     * @return string
     */
    public function getResourceForeignKey(): string;

    /**
     * Get resource ID strategy.
     *
     * @return ResourceIdStrategyInterface
     */
    public function getResourceIdStrategy(): ResourceIdStrategyInterface;

    /**
     * Get resource key for a single item.
     *
     * @return string
     */
    public function getResourceItemKey(): string;

    /**
     * Get resource limit.
     * Used to limit number of resources per page.
     *
     * @return int
     */
    public function getResourceLimit(): int;

    /**
     * Get resource primary key.
     * Used to retrieve model in database.
     *
     * @return string
     */
    public function getResourcePrimaryKey(): string;

    /**
     * Get resource fields used to perform search on it.
     *
     * @return array
     */
    public function getSearchFields(): array;

    /**
     * Get transformer.
     *
     * @return ResourceTransformerInterface
     */
    public function getTransformer(): ResourceTransformerInterface;

    /**
     * Get array representation of resource.
     *
     * @return array
     */
    public function toArray(): array;

    /**
     * Get validate model based on validation set.
     *
     * @param string $set
     *
     * @return bool
     *
     * @throws BaseException If no rules property defined
     *                       If rules are not an array
     * @throws ValidationFailedException
     */
    public function validate(string $set): bool;
}
