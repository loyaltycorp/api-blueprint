<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Interfaces;

use Illuminate\Http\JsonResponse;

interface ResourceControllerInterface
{
    const ACTION_STORE = 'store';
    const ACTION_UPDATE = 'update';

    /**
     * Destroy a single resource.
     *
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     */
    public function destroy($primaryKey, $parentKey = null): JsonResponse;

    /**
     * Get paginated list of resources.
     *
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     */
    public function index($parentKey = null): JsonResponse;

    /**
     * Get a single resource.
     *
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     */
    public function show($primaryKey, $parentKey = null): JsonResponse;

    /**
     * Create and store a single resource.
     *
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     */
    public function store($parentKey = null): JsonResponse;

    /**
     * Update a single resource.
     *
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     */
    public function update($primaryKey, $parentKey = null): JsonResponse;
}
