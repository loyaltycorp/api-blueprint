<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Interfaces;

interface ResourceTransformerInterface
{
    /**
     * Transform resource to array.
     *
     * @param ResourceInterface $resource
     *
     * @return array
     */
    public function transform(ResourceInterface $resource): array;
}
