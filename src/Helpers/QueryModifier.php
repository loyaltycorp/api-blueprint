<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Helpers;

use Closure;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\StrInterface;
use LoyaltyCorp\ApiBlueprint\Helpers\Interfaces\QueryModifierInterface;

class QueryModifier implements QueryModifierInterface
{
    /**
     * @var string
     */
    private $defaultOrderBy;

    /**
     * @var array
     */
    private $filterFields;

    /**
     * @var array
     */
    private $searchFields;

    /** @var StrInterface */
    private $str;

    /**
     * @param array             $filtersFields
     * @param string            $defaultOrderBy
     * @param array             $searchFields
     * @param StrInterface|null $str
     */
    public function __construct(
        array $filtersFields,
        string $defaultOrderBy,
        array $searchFields,
        StrInterface $str = null
    ) {
        $this->filterFields = $filtersFields;
        $this->defaultOrderBy = $defaultOrderBy;
        $this->searchFields = $searchFields;
        $this->str = $str ?? \container(StrInterface::class);
    }

    /**
     * Apply where conditions on resource query based on given filters.
     *
     * @param Relation|ResourceModel $query
     * @param array                  $filters
     *
     * @return QueryModifierInterface
     */
    public function applyFilters($query, array $filters): QueryModifierInterface
    {
        foreach ($filters as $filter => $value) {
            if (!\in_array($filter, $this->filterFields, true)) {
                continue;
            }

            if ($this->str->startsWith($value, '[') && $this->str->endsWith($value, ']')) {
                $explode = \explode(',', \str_replace(['[', ']'], '', $value));

                if (\count($explode) > 1) {
                    //filters[<name>]=[<operator>, <value>, <boolean>]
                    $query->where($filter, $explode[0], $explode[1], $explode[2] ?? 'and');

                    continue;
                }

                //filters[<name>]=[<value>] - operator set to equal
                $query->where([$filter => $explode[0]]);

                continue;
            }

            if ($this->str->contains($value, ',')) {
                //filters[<name>]=<value1>,<value2>,...,<valueN> - where in
                $query->whereIn($filter, \explode(',', $value));

                continue;
            }

            //filters[<name>]=<value>
            $query->where([$filter => $value]);
        }

        return $this;
    }

    /**
     * Apply orderBy on resource query.
     *
     * @param Relation|ResourceModel $query
     * @param null|string            $orderBy
     *
     * @return QueryModifierInterface
     */
    public function applyOrderBy($query, string $orderBy = null): QueryModifierInterface
    {
        $explode = \explode(',', $orderBy ?? $this->defaultOrderBy);

        $field = $explode[0];
        $direction = $explode[1] ?? 'asc';

        if (!\in_array($field, $this->filterFields, true)) {
            return $this;
        }

        $query->orderBy($field, $direction);

        return $this;
    }

    /**
     * Apply where conditions on resource query based on given search.
     *
     * @param Relation|ResourceModel $query
     * @param null|string            $search
     *
     * @return QueryModifierInterface
     */
    public function applySearch($query, string $search = null): QueryModifierInterface
    {
        if (null !== $search) {
            $query->where($this->getSearchClosure($search));
        }

        return $this;
    }

    /**
     * @param string $search
     *
     * @return Closure
     */
    private function getSearchClosure(string $search): Closure
    {
        return function (Builder $builder) use ($search) {
            foreach ($this->searchFields as $field) {
                $builder->orWhere($field, 'like', '%' . $search . '%');
            }
        };
    }
}
