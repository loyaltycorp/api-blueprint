<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Helpers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use LoyaltyCorp\ApiBlueprint\Helpers\Interfaces\PaginatedResponseBuilderInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface;

class PaginatedResponseBuilder implements PaginatedResponseBuilderInterface
{
    /**
     * @var LengthAwarePaginator
     */
    private $paginator;

    /**
     * @var string
     */
    private $resourceKey;

    /**
     * @var ResourceTransformerInterface
     */
    private $transformer;

    /**
     * @param LengthAwarePaginator $paginator
     * @param ResourceTransformerInterface $transformer
     * @param string $resourceKey
     */
    public function __construct(
        LengthAwarePaginator $paginator,
        ResourceTransformerInterface $transformer,
        string $resourceKey
    ) {
        $this->paginator = $paginator;
        $this->transformer = $transformer;
        $this->resourceKey = $resourceKey;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getPaginator(): LengthAwarePaginator
    {
        return $this->paginator;
    }

    /**
     * @return string
     */
    public function getResourceKey(): string
    {
        return $this->resourceKey;
    }

    /**
     * @return ResourceTransformerInterface
     */
    public function getTransformer(): ResourceTransformerInterface
    {
        return $this->transformer;
    }
}
