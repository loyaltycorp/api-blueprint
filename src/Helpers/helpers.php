<?php declare(strict_types = 1);

if (!function_exists('container')) {
    /**
     * Get service instance from service container.
     *
     * @param string $service
     *
     * @return mixed
     */
    function container(string $service)
    {
        return \app()->make($service);
    }
}
