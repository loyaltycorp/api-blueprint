<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Helpers\Interfaces;

use Illuminate\Database\Eloquent\Relations\Relation;
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;

interface QueryModifierInterface
{
    /**
     * Apply where conditions on resource query based on given filters.
     *
     * @param Relation|ResourceModel $query
     * @param array                  $filters
     *
     * @return self
     */
    public function applyFilters($query, array $filters): self;

    /**
     * Apply orderBy on resource query.
     *
     * @param Relation|ResourceModel $query
     * @param null|string            $orderBy
     *
     * @return self
     */
    public function applyOrderBy($query, string $orderBy = null): self;

    /**
     * Apply where conditions on resource query based on given search.
     *
     * @param Relation|ResourceModel $query
     * @param null|string            $search
     *
     * @return self
     */
    public function applySearch($query, string $search = null): self;
}
