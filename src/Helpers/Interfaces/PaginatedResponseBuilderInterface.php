<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Helpers\Interfaces;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface;

interface PaginatedResponseBuilderInterface
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginator(): LengthAwarePaginator;

    /**
     * @return string
     */
    public function getResourceKey(): string;

    /**
     * @return ResourceTransformerInterface
     */
    public function getTransformer(): ResourceTransformerInterface;
}
