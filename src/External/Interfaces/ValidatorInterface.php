<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\External\Interfaces;

use Illuminate\Validation\Validator;

interface ValidatorInterface
{
    /**
     * Create validator instance.
     *
     * @param array $attributes The list of attributes to validate
     * @param array $rules      The list of validation rules
     *
     * @return Validator
     */
    public function make(array $attributes, array $rules): Validator;
}
