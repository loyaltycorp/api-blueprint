<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\External\Interfaces;

interface StrInterface
{
    /**
     * Determine if a given string contains a given substring.
     *
     * @param  string       $haystack
     * @param  string|array $needles
     *
     * @return bool
     */
    public function contains(string $haystack, $needles): bool;

    /**
     * Determine if a given string ends with a given substring.
     *
     * @param  string       $haystack
     * @param  string|array $needles
     *
     * @return bool
     */
    public function endsWith(string $haystack, $needles): bool;

    /**
     * Convert the given string to lower-case.
     *
     * @param  string $value
     *
     * @return string
     */
    public function lower(string $value): string;

    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string      $title
     * @param  null|string $separator
     * @param  null|string $language
     *
     * @return string
     */
    public function slug(string $title, string $separator = null, string $language = null): string;

    /**
     * Convert a string to snake case.
     *
     * @param  string      $value
     * @param  null|string $delimiter
     *
     * @return string
     */
    public function snake(string $value, string $delimiter = null): string;

    /**
     * Determine if a given string starts with a given substring.
     *
     * @param  string       $haystack
     * @param  string|array $needles
     *
     * @return bool
     */
    public function startsWith(string $haystack, $needles): bool;
}
