<?php
declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\External\Libraries;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\CollectionInterface;

abstract class Collection extends EloquentCollection implements CollectionInterface
{

}
