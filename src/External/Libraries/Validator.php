<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\External\Libraries;

use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\Validator as IlluminateValidator;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\ValidatorInterface;

class Validator implements ValidatorInterface
{
    /**
     * Create validator instance.
     *
     * @param array $attributes The list of attributes to validate
     * @param array $rules      The list of validation rules
     *
     * @return IlluminateValidator
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function make(array $attributes, array $rules): IlluminateValidator
    {
        return ValidatorFacade::make($attributes, $rules);
    }
}
