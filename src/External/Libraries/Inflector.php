<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\External\Libraries;

use Doctrine\Common\Inflector\Inflector as DoctrineInflector;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\InflectorInterface;

class Inflector implements InflectorInterface
{
    /**
     * Returns a word in plural form.
     *
     * @param string $word The word in singular form.
     *
     * @return string The word in plural form.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function pluralize(string $word): string
    {
        return DoctrineInflector::pluralize($word);
    }
}
