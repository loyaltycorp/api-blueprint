<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\External\Libraries;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\ModelInterface;

abstract class Model extends EloquentModel implements ModelInterface
{

}
