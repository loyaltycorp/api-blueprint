<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\External\Libraries;

use Illuminate\Support\Collection;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;
use League\Fractal\Resource\ResourceInterface as FractalResourceInterface;
use League\Fractal\Scope;
use League\Fractal\Serializer\SerializerAbstract;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\FractalInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface;

class Fractal extends Manager implements FractalInterface
{
    /**
     * @param Collection                   $collection
     * @param ResourceTransformerInterface $transformer
     * @param string                       $resourceKey
     *
     * @return FractalCollection
     */
    public function createCollection(
        Collection $collection,
        ResourceTransformerInterface $transformer,
        string $resourceKey
    ): FractalCollection {
        return new FractalCollection($collection, $transformer, $resourceKey);
    }

    /**
     * Create Data.
     *
     * Main method to kick this all off. Make a resource then pass it over, and use toArray()
     *
     * @param FractalResourceInterface $resource
     * @param string                   $scopeIdentifier
     * @param Scope                    $parentScopeInstance
     *
     * @return Scope
     */
    public function createData(
        FractalResourceInterface $resource,
        $scopeIdentifier = null,
        Scope $parentScopeInstance = null
    ): Scope {
        return parent::createData($resource, $scopeIdentifier, $parentScopeInstance);
    }

    /**
     * @param ResourceInterface            $resource
     * @param ResourceTransformerInterface $transformer
     * @param string                       $resourceKey
     *
     * @return FractalItem
     */
    public function createItem(
        ResourceInterface $resource,
        ResourceTransformerInterface $transformer,
        string $resourceKey
    ): FractalItem {
        return new FractalItem($resource, $transformer, $resourceKey);
    }

    /**
     * Parse Exclude String.
     *
     * @param array|string $excludes Array or csv string of resources to exclude
     *
     * @return FractalInterface
     */
    public function parseExcludes($excludes): FractalInterface
    {
        parent::parseExcludes($excludes);

        return $this;
    }

    /**
     * Parse Include String.
     *
     * @param array|string $includes Array or csv string of resources to include
     *
     * @return FractalInterface
     */
    public function parseIncludes($includes): FractalInterface
    {
        parent::parseIncludes($includes);

        return $this;
    }

    /**
     * @param array|string $includes Array or csv string of resources to include
     * @param array|string $excludes Array or csv string of resources to exclude
     *
     * @return FractalInterface
     */
    public function parseIncludesAndExcludes($includes, $excludes): FractalInterface
    {
        $this->parseIncludes($includes);
        $this->parseExcludes($excludes);

        return $this;
    }

    /**
     * Set Serializer
     *
     * @param SerializerAbstract $serializer
     *
     * @return FractalInterface
     */
    public function setSerializer(SerializerAbstract $serializer): FractalInterface
    {
        parent::setSerializer($serializer);

        return $this;
    }
}
