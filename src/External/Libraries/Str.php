<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\External\Libraries;

use Illuminate\Support\Str as IlluminateStr;
use LoyaltyCorp\ApiBlueprint\External\Interfaces\StrInterface;

class Str implements StrInterface
{
    /**
     * Determine if a given string contains a given substring.
     *
     * @param  string       $haystack
     * @param  string|array $needles
     *
     * @return bool
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function contains(string $haystack, $needles): bool
    {
        return IlluminateStr::contains($haystack, $needles);
    }

    /**
     * Determine if a given string ends with a given substring.
     *
     * @param  string       $haystack
     * @param  string|array $needles
     *
     * @return bool
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function endsWith(string $haystack, $needles): bool
    {
        return IlluminateStr::endsWith($haystack, $needles);
    }

    /**
     * Convert the given string to lower-case.
     *
     * @param  string $value
     *
     * @return string
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function lower(string $value): string
    {
        return IlluminateStr::lower($value);
    }

    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string      $title
     * @param  null|string $separator
     * @param  null|string $language
     *
     * @return string
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function slug(string $title, string $separator = null, string $language = null): string
    {
        return IlluminateStr::slug($title, $separator ?? '-', $language ?? 'en');
    }

    /**
     * Convert a string to snake case.
     *
     * @param  string      $value
     * @param  null|string $delimiter
     *
     * @return string
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function snake(string $value, string $delimiter = null): string
    {
        return IlluminateStr::snake($value, $delimiter ?? '_');
    }

    /**
     * Determine if a given string starts with a given substring.
     *
     * @param  string       $haystack
     * @param  string|array $needles
     *
     * @return bool
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function startsWith(string $haystack, $needles): bool
    {
        return IlluminateStr::startsWith($haystack, $needles);
    }
}
