<?php declare(strict_types=1);

namespace LoyaltyCorp\ApiBlueprint\Transformers;

use Illuminate\Support\Collection;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\NullResource;
use League\Fractal\TransformerAbstract;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceInterface;
use LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface;

class ResourceTransformer extends TransformerAbstract implements ResourceTransformerInterface
{
    /**
     * Transform resource to array.
     *
     * @param ResourceInterface $resource
     *
     * @return array
     */
    public function transform(ResourceInterface $resource): array
    {
        return $resource->toArray();
    }

    /**
     * Include collection of resources.
     *
     * @param ResourceInterface $resource
     * @param Collection        $collection
     *
     * @return FractalCollection
     */
    protected function includeCollection(ResourceInterface $resource, Collection $collection): FractalCollection
    {
        /** @var TransformerAbstract $transformer */
        $transformer = $resource->getTransformer();

        return $this->collection($collection, $transformer, $resource->getResourceCollectionKey());
    }

    /**
     * Include one resource.
     *
     * @param ResourceInterface $resource
     *
     * @return Item|NullResource
     */
    protected function includeResource(ResourceInterface $resource = null)
    {
        if (null === $resource) {
            return $this->null();
        }

        /** @var TransformerAbstract $transformer */
        $transformer = $resource->getTransformer();

        return $this->item($resource, $transformer, $resource->getResourceItemKey());
    }
}
