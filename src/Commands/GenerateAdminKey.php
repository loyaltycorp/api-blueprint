<?php declare(strict_types = 1);

namespace LoyaltyCorp\ApiBlueprint\Commands;

use Illuminate\Console\Command;
use LoyaltyCorp\ApiBlueprint\Services\Keys\Interfaces\KeyGeneratorInterface;

class GenerateAdminKey extends Command
{
    /** @var string */
    protected $description = 'Generate new admin api key';

    /** @var string */
    protected $signature = 'api:generate-admin-key';

    /** @var KeyGeneratorInterface */
    private $keyGenerator;

    /**
     * @param KeyGeneratorInterface $keyGenerator
     */
    public function __construct(KeyGeneratorInterface $keyGenerator)
    {
        parent::__construct();

        $this->keyGenerator = $keyGenerator;
    }

    /**
     * Generate a new admin api key using the key generator service.
     */
    public function handle()
    {
        $key = $this->keyGenerator->generate('admin');

        $this->info('New admin api key: ' . $key);
    }
}
