API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Implementation
--------------

####Package Classes
For the package to work properly with your application, the objects MUST extend/implement the appropriate class/interface:

| Type | Class to extend | Interface to implement |
|------|-----------------|------------------------|
| Model | `LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel` | `LoyaltyCorp\ApiBlueprint\Interfaces\ResourceInterface` |
| Repository | `LoyaltyCorp\ApiBlueprint\Database\Repositories\ResourceRepository` | `LoyaltyCorp\ApiBlueprint\Interfaces\ResourceRepositoryInterface` |
| Transformer | `LoyaltyCorp\ApiBlueprint\Transformers\ResourceTransformer` | `LoyaltyCorp\ApiBlueprint\Interfaces\ResourceTransformerInterface` |
| Exception | `LoyaltyCorp\ApiBlueprint\Exceptions\BaseException` | `LoyaltyCorp\ApiBlueprint\Interfaces\ExceptionInterface` |
| IdStrategy | - | `LoyaltyCorp\ApiBlueprint\Interfaces\ResourceIdStrategyInterface` |

For the controllers and exception handlers the implementation depends on the application you project is based on:

| Type | Class to extend | Interface to implement |
|------|-----------------|------------------------|
| Controller | `LoyaltyCorp\ApiBlueprint\Controllers\LumenResourceController` | `LoyaltyCorp\ApiBlueprint\Interfaces\ResourceControllerInterface` |
| Controller | `LoyaltyCorp\ApiBlueprint\Controllers\LaravelResourceController` | `LoyaltyCorp\ApiBlueprint\Interfaces\ResourceControllerInterface` |
| ExceptionHandler | `LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\LumenExceptionHandler` | - |
| ExceptionHandler | `LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\LaravelExceptionHandler` | - |

####Models
First thing to do when you create a model is to extend the `ResourceModel`. 
It declares an abstract function `getValidationRules(): array` that you HAVE to implement. This function has for purpose
to define the validation rules for the current object:

```php
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;

class MyObject extends ResourceModel
{
    protected $fillable = [
        'fillable1',
        'fillable2'
    ];
    
    protected function getValidationRules(): array
    {
        return [
            'store' => [
                'fillable1' => 'required|string',
                'fillable2' => 'required|string'
            ],
            'update' => [
                // ...
            ]
        ];
    }
}
```

The `ResourceModel` class provides a way to easily configure your models behavior by properties or directly overriding
methods:

| Method | Type | Format | Default | Description |
|--------|------|--------|---------|-------------|
| `getTransformer()` | string | [Fully qualified class name](http://php.net/manual/en/language.namespaces.rules.php) | `LoyaltyCorp\ApiBlueprint\Transformers\ResourceTransformer` | Fully qualified class name of the transformer used by the model |
| `getSearchFields()` | array | `['field1', 'field2', ..., 'fieldN']` | Model's fillables | List of fields enabled for searching |
| `getFilterFields()` | array | `['field1', 'field2', ..., 'fieldN']` | Model's fillables | List of fields enabled for filtering |
| `getOrderByAndDirection()` | string | `'field,direction'` | Field and direction used to order by database records of the model |

####Repositories
Once your model created, you have to create the associated repository.
First thing to do when you create a model is to extend the `ResourceRepository`.
It declares an abstract function `getResourceClass(): string` that you HAVE to implement. This function has for purpose
to define the resource managed by the current repository:

```php
use LoyaltyCorp\ApiBlueprint\Database\Repositories\ResourceRepository;
use App\Database\Models\MyObject;

class MyObjectRepository extends ResourceRepository
{
    /**
     * Get resource class managed by the repository.
     *
     * @return string
     */
     protected function getResourceClass(): string
     {
         return MyObject::class;
     }
}
```

In the case of a repository managing a sub-model you HAVE to define the parent class and the relation used by the
parent to interact with the resource as following:

```php
use LoyaltyCorp\ApiBlueprint\Database\Repositories\ResourceRepository;
use App\Database\Models\MyParents\MyObject;
use App\Database\Models\MyParent;
 
class MyParentMyObjectRepository extends ResourceRepository
{
     /**
      * Get resource class managed by the repository.
      *
      * @return string
      */
      protected function getResourceClass(): string
      {
          return MyObject::class;
      }
      
      /**
       * Get parent class managed by the repository.
       * It can be null when dealing with root resources.
       *
       * @return null|string
       */
       protected function getParentClass(): ?string
       {
           return MyParent::class;
       }
       
       /**
        * Get relation name on parent.
        * It can be null when dealing with root resources.
        *
        * @return null|string
        */
        protected function getParentRelation(): ?string
        {
            return 'myObject';
        }
}
```

####Controllers
This package defines two resource controllers, one for [Lumen](https://lumen.laravel.com/) and one for 
[Laravel](https://laravel.com/). The reason is because the two frameworks are not using the same routing logic but 
no worries both of controllers of this package provide the same configuration and flexibility, just make sure to 
extend the one related to the framework you are using:

- `LoyaltyCorp\ApiBlueprint\Controllers\LaravelResourceController`
- `LoyaltyCorp\ApiBlueprint\Controllers\LumenResourceController`


First thing to do when you create a controller is to extend the package controller class.
It declares an abstract function `getRepositoryClass(): string` that you HAVE to implement. This function has for purpose
to define the repository used by the current controller:

```php
use LoyaltyCorp\ApiBlueprint\Database\Repositories\MyObjectRepository;

class MyObjectsController extends LumenResourceController
{
    /**
     * Defines repository used by the current controller.
     *
     * @return string
     */
     protected function getRepositoryClass(): string
     {
         return MyObjectRepository::class;
     }
}
```
