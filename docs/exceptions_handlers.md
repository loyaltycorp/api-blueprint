API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Exceptions and Handlers
-----------------------

####Exceptions
This package defines a `LoyaltyCorp\ApiBlueprint\Interfaces\ExceptionInterface` with a toResponse method which returns
a JsonResponse. This package also defines a `LoyaltyCorp\ApiBlueprint\Exceptions\BaseException` which implements the previous
`ExceptionInterface`.

A good practice is to create a base exception class in your project and extend the `BaseException` provided by the package.
This way you ensure that all your exceptions provide a way to return a well json formatted response to the user.

The `BaseException` provides properties to easily customise the generated response:
* __message:__ Message displayed in production environment
* __code:__ Internal code attached to the exception
* __statusCode:__ Status code of the generated response
* __extendedMessage:__ Message displayed in debug environment

The `BaseException` provides a constructor to override extendedMessage, code and statusCode:
```php
    /**
     * BaseException constructor.
     *
     * @param null|string     $extendedMessage
     * @param null|int|string $code
     * @param null|int        $statusCode
     */
     public function __construct($extendedMessage = null, $code = null, $statusCode = null);
```

| Exception | Code | StatusCode | Reason |
|-----------|------|------------|--------|
| `ForbiddenException` | AP-AU-FO | 403 | Access Denied |
| `UnauthorisedException` | AP-AU-UN | 401 | Access to the resource is restricted |
| `InvalidRelationException` | RE-IN-RE | 400 | In a ResourceController, the relation between two resources is not defined or does not exist |
| `InvalidResourceException` | RE-IN | 400 | In a ResourceController, the resource class is not defined, does not exist or does not implement `ResourceInterface` |
| `ResourceErrorException` | RE-AC | 500 | An error occurred during resource action (`save()`, `update()`, ...) |
| `ResourceNotFoundException` | RE-NO-FO | 404 | A particular resource's instance does not exist in the database |
| `ValidationFailedException ` | RE-VA-FA | 400 | A resource was instantiated with invalid data |

####Handlers
This package defines two exception handlers, one for [Lumen][1] and one for [Laravel][2]. The reason is because the
two frameworks are not using the same exception handling logic but no worries both of handlers of this package provide 
the same service, just make sure to extend the one related to the framework you are using:
- `LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\LaravelExceptionHandler`
- `LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\LumenExceptionHandler`

To use exception handler in your project, simply create an exception handler class and extend the handler related to the
framework you are using. It implements the `render` method for you using the `ExceptionInterface` of this package to
return well json formatted responses to the user as following:

```php
use LoyaltyCorp\ApiBlueprint\Exceptions\Handlers\LumenExceptionHandler;

class MyHandler extends LumenExceptionHandler
{
    /**
         * A list of the exception types that should not be reported.
         *
         * @var array
         */
        protected $dontReport = [
            // Exception classes...
        ];
    
        /**
         * @param Exception $exception
         */
        public function report(Exception $exception): void
        {
            // Perform your reporting logic...
             
            parent::report($exception);
        }
} 
```
