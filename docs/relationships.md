API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Relationships
-------------

By default the package is able to handle most of relationships once they are defined in models, no need to customise
your controllers.

`ManyToMany` relationship will require your controllers to be override but the `ResourceRepository` provides you
everything you need.

When you want to detach a model from a `ManyToMany` no action is required.

####ManyToMany
In a `ManyToMany` case you want to link two existing models, update them basically so you will have to override `update`
method of your controller. Because the models are not linked yet you can't use the `ResourceRepository::get()` method
because it use the relation on the parent to retrieve the model, to handle this use 
`ResourceRepository::getWithoutParent()` instead.

Once the model you want to attach to the `ManyToMany` retrieved you can simply use `ResourceRepository::save($model)`
which is able to handle perfectly this relationship use case.

```php
class MyObjectsController extends LumenResourceController
{
    // ...
    
    /**
     * Update a single resource.
     *
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     */
     public function update($primaryKey, $parentKey = null): JsonResponse
     {
        $repository = $this->getRepository($primaryKey, $parentKey);
        
        $repository->save($repository->getWithoutParent());
        
        return $this->emptyResponse();
     }
    
    // ...
}
``` 

####ManyToMany with extra attributes
In a `ManyToMany` with extra attributes use case, you have to validate them yourself using controllers methods.
Once validation done, `ResourceRepository::save($model, array $options = null)` provides a second parameter which an
array to handle eloquent saving options or attributes on relationships.

```php
class MyObjectsController extends LumenResourceController
{
    // ...
    
    /**
     * Update a single resource.
     *
     * @param string|int      $primaryKey
     * @param null|string|int $parentKey
     *
     * @return JsonResponse
     */
     public function update($primaryKey, $parentKey = null): JsonResponse
     {
        // Retrieve extra attributes from request
        $extraAttributes = ...
        // Retrieve your validation rules
        $rules = ...
        
        $this->validate($extraAttributes, $rules);
        
        $repository = $this->getRepository($primaryKey, $parentKey);
        
        $repository->save($repository->getWithoutParent(), $extraAttributes);
        
        return $this->emptyResponse();
     }
    
    // ...
}
``` 
