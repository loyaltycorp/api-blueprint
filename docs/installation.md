API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Installation
------------

####Requirements
Before installing the package must meet the following requirements:
* PHP >= 7.1

####Composer
Require the package in your project as following:

```
$ composer require loyaltycorp/api-blueprint
```

####Lumen
For the package to work properly in a [Lumen](https://lumen.laravel.com/) project you need to register the 
service provider:

```php
// bootstrap/app.php

// ...

$app->register(LoyaltyCorp\ApiBlueprint\Providers\ApiBlueprintServiceProvider::class);

// ...
```

And you need to enable [Eloquent](https://laravel.com/docs/master/eloquent):

```php
// bootstrap/app.php

// ...

$app->withEloquent();

// ...
```

####Laravel
For the package to work properly in a [Laravel](https://laravel.com/) project you need to register the 
service provider:

```php
// config/app.php

'providers' => [
    // Other Service Providers

    LoyaltyCorp\ApiBlueprint\Providers\ApiBlueprintServiceProvider::class,
],
```
