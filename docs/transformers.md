API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Transformers
------------

The transformers are using the [Fractal Library](https://fractal.thephpleague.com/transformers/).

####Includes

The `ResourceTransformer` implements two functions to make easier to include 'ResourceInterface' or collection of it:

- `protected function includeCollection(ResourceInterface $resource, Collection $collection): FractalCollection`
- `protected function includeResource(ResourceInterface $resource = null)`

####Include Collection

To include a collection of `ResourceInterface` use the `includeCollection` function as following:

```php
use LoyaltyCorp\ApiBlueprint\Transformers\ResourceTransformer;
use App\Database\Models\MyObject;
use App\Database\Models\MyObjects\MyChild;

class MyTransformer extends ResourceTransformer
{
    protected $availableIncludes = [
        'myChildren'
    ];
    
    public function includeMyCollection(MyObject $resource)
    {
        return $this->includeCollection(new MyChild(), $resource->myChildren);
    }
}
```

####Include Resource

To include a single `ResourceInterface` use the `includeResource` function as following:

```php
use LoyaltyCorp\ApiBlueprint\Transformers\ResourceTransformer;
use App\Database\Models\MyObject;

class MyTransformer extends ResourceTransformer
{
    protected $availableIncludes = [
        'myChild'
    ];
    
    public function includeMyCollection(MyObject $resource)
    {
        return $this->includeResource($resource->myChild);
    }
}
```
