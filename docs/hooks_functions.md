API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Hooks Functions
---------------

The package controller classes declare action hooks functions to perform actions on the current model object 
before/after saving/deleting is performed:

- `protected function beforeSaving(ResourceModel $current, string $action, $parentKey = null): void`
- `protected function afterSaving(ResourceModel $current, string $action, $parentKey = null): void`
- `protected function beforeDeleting(ResourceModel $current, $parentKey = null): void`
- `protected function afterDeleting(ResourceModel $current, $parentKey = null): void`

####Saving Actions
The `$action` parameter passed to saving hooks functions could take two different values which are defined as constants
on the package controller interface as following:

- `ResourceControllerInterface::ACTION_STORE`
- `ResourceControllerInterface::ACTION_UPDATE`


You can check this parameter value to perform action only on store or update as following:
```php
use LoyaltyCorp\ApiBlueprint\Controllers\LumenResourceController;

class MyController extends LumenResourceController
{
    // ...
    
    /**
     * Perform actions after resource saving.
     *
     * @param ResourceModel   $current
     * @param string          $action
     * @param null|int|string $parentKey
     *
     * @return void
     */
     protected function afterSaving(ResourceModel $current, string $action, $parentKey = null): void
     {
        // Check if action is update
        if ($action === self::ACTION_UPDATE) {
            // Perform action on $current
        }
     }
}
```

####Deleting Actions
The `$action` parameter is not passed to deleting hooks functions because there is only one situation when the model
is deleted. 
