API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Routing
-------

Every route must define the `primaryKey` parameter which is the primary key of the target resource. 
Every route must define prefix and namespace to follow the controllers and models structures.

> If the resource is a child of another one then the route must define the `parentKey` parameter which
> is the primary key of the direct parent.
