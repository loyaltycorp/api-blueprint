API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Files Convention
----------------

####Naming Convention

######Models
The naming convention for the controllers is: [Model].php 

######Repositories
The naming convention for the repositories is: [Model]Repository.php. 
The repository of a sub-object starts with the name of the parent as: [Parent][Model]Repository.php

######Controllers
The naming convention for the controllers is: [Models]Controller.php 

####Namespaces

All controllers, models and repositories have a namespace following the RESTful structure and the PSR-4 autoloading
specifications. It is highly recommend to follow the same structure in your projects. Example with restaurant which 
has addresses:

| Type | Namespace | Path |
|------|-----------|------|
| Model | `App\Database\Models\Restaurant` | src/Database/Models/Restaurant.php |
| Model | `App\Database\Models\Restaurants\Address` | src/Database/Models/Restaurants/Address.php |
| Repository | `App\Database\Repositories\RestaurantRepository` | src/Database/Repositories/RestaurantRepository.php |
| Repository | `App\Database\Repositories\Restaurants\RestaurantAddressRepository` | src/Database/Repositories/Restaurants/RestaurantAddressRepository.php |
| Controller | `App\Http\Controllers\RestaurantsController` | src/Http/Controllers/RestaurantsController.php |
| Controller | `App\Http\Controllers\Restaurants\AddressesController` | src/Http/Controllers/Restaurants/AddressesController.php |
