API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Example
-------

Example with Restaurant and Address.

####Models
######Restaurant
```php
class Restaurant extends ResourceModel 
{
    // Relation
    public function addresses()
    {
        return $this->hasMany(Address::class, $this->getResourceForeignKey());
    }
    
    // Transformer
    public function getTransformer(): ResourceTransformerInterface
    {
        return new RestaurantTransformer();
    }

    // Validation rules
    protected function getValidationRules(): array
    {
        return [
            'default' => [
                //Rules...    
            ],
            'update' => [
                //Rules...
            ]
        ];
    }
}
```

######Restaurants/Address
```php
class Address extends ResourceModel 
{
    // Validation rules
    protected function getValidationRules(): array
    {
        return [
            'default' => [
                //Rules...    
            ],
            'update' => [
                //Rules...
            ]
        ];
    }
}
```

####Repositories
######RestaurantRepository

```php
class RestaurantRepository extends ResourceRepository
{
    public function getResourceClass(): string
    {
        retun Restaurant::class;
    }
}
```

######RestaurantAddressRepository

```php
class RestaurantAddressRepository extends ResourceRepository
{
    public function getParentClass(): ?string
    {
        return Restaurant::class;
    }
        
    public function getParentRelation(): ?string
    {
        return 'addresses';
    }
            
    public function getResourceClass(): string
    {
        return Address::class;
    }
}
```

####Controllers
######RestaurantsController
```php
class RestaurantsController extends LumenResourceController
{
    public function getRepositoryClass(): string
    {
        return RestaurantRepository::class;
    }
}
```

######Restaurants/AddressesController
```php
class AddressesController extends LumenResourceController
{
    public function getRepositoryClass(): string
    {
        return RestaurantAddressRepository::class;
    }
}
```

####Transformers
```php
class RestaurantTransformer extends ResourceTransformer
{
    protected $availableIncludes = [
        'addresses'
    ];
    
    public function includeAddresses(Restaurant $restaurant)
    {
        $address = new Address();
    
        return $this->includeCollection($address, $restaurant->addresses);
    }
}
```

####Routing
```php
/** Public group */
$router->group(['middleware' => ['auth']], function () use ($router) {
    $router->group(['prefix' => 'restaurants'], function () use ($router) {
        /** Restaurant */
        $router->get('/', 'RestaurantsController@index');
        $router->get('/{primaryKey}', 'RestaurantsController@show');

        /** Restaurant/Address */
        $router->group(['prefix' => '/{parentKey}/addresses', 'namespace' => 'Restaurants'], function () use ($router) {
            $router->get('/', 'AddressesController@index');
            $router->get('/{primaryKey}', 'AddressesController@show');
        });
    });
});
/** Restaurant group */
$router->group(['prefix' => 'restaurants', 'middleware' => ['auth', 'restaurant']], function () use ($router) {
    /** Restaurant */
    $router->put('/{primaryKey}', 'RestaurantsController@update');

    /** Restaurant/Address */
    $router->group(['prefix' => '/{parentKey}/addresses', 'namespace' => 'Restaurants'], function () use ($router) {
        $router->post('/', 'AddressesController@store');
        $router->put('/{primaryKey}', 'AddressesController@update');
        $router->delete('/{primaryKey}', 'AddressesController@destroy');
    });
});
```
