API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

Unit Tests
----------

The package tries to avoid as much as possible any dependency with the frameworks but some features still rely on them
that's why it extends [Lumen](https://lumen.laravel.com/) test case to perform its unit tests. All framework features
used by this package are wrapped in external libraries interfaces trying to make it framework agnostic. But when it
comes to use them in your unit tests you still have to configure the framework to work properly.

####Create Application

The `TestCase` class from [Lumen](https://lumen.laravel.com/) defines a `createApplication` function which return an
instance of a Lumen application. By default the package override this function to register its services in the application:

```php
    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return Application
     */
    public function createApplication()
    {
        $application = new Application(\realpath(__DIR__));

        $application->register(ApiBlueprintServiceProvider::class);

        return $application;
    }
```

######Facades
When it comes to use some package's external libraries in your unit tests, you need to override the `createApplication`
function to configure the application to use facades:

```php
    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return Application
     */
    public function createApplication()
    {
        $application = parent::createApplication();
        $application->withFacades();

        return $application;
    }
```

######Database
If you want to use the database in your unit tests you need to override the `createApplication` function to configure
the application to use eloquent:

```php
    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return Application
     */
    public function createApplication()
    {
        $application = parent::createApplication();
        $application->withEloquent();

        return $application;
    }
```
