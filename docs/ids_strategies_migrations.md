API Blueprint
=============

> Package which provides the magic to handle most of common use
> cases while building a RESTful API using [Lumen](https://lumen.laravel.com/) or [Laravel](https://laravel.com/).

---

IDs Strategy and Migrations
---------------------------

####Unpredictable IDs Strategy
Because the IDs are exposed in the URLs, the default strategy of the package is to make complicated to predict
the sequence, state or structure of the database from it. To accomplish it, by default the IDs of the objects extending
the `ResourceModel` are not using the increment SQL.
They are strings of 13 random alphanumeric characters as following: `5a0a257ae2eb4`.


This strategy require to implement the [String IDs Migrations](#markdown-header-string-ids-migrations).

####AutoIncrement IDs Strategy
This strategy simply uses SQL [AUTO_INCREMENT Field](https://www.w3schools.com/sql/sql_autoincrement.asp). To use it in
your project, simply override `ResourceModel::getResourceIdStrategy()` and return a new instance of 
`LoyaltyCorp\ApiBlueprint\Database\IdStrategies\AutoIncrementStrategy`:

```php
use LoyaltyCorp\ApiBlueprint\Database\Models\ResourceModel;

class YourModel extends ResourceModel
{
    // ...
    
    /**
     * Get resource ID strategy.
     *
     * @return ResourceIdStrategyInterface
     */
     public function getResourceIdStrategy(): ResourceIdStrategyInterface
     {
        return new AutoIncrementStrategy();
     }
    
    // ...
}
```

####String IDs Migrations
IDs are defined as string in the migrations as following:

```php
class CreateParentModelStubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_model_stubs', function (Blueprint $table) {
            $table->string('id')->primary();
            
            // Rest of migration
        });
    }
    
    // ...
}
``` 

Foreign keys are defined as string in the migrations as following:

```php
class CreateParentModelStubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_model_stubs', function (Blueprint $table) {
            // ...
            
            $table->string('foreign_key');
            
            $table
                ->foreign('foreign_key')
                ->references('id')
                ->on('foreign_table')
            ;
            
            // Rest of migration
        });
    }
    
    // ...
}
``` 

####Increments IDs Migrations
IDs are defined as increments in the migrations as following:

```php
class CreateParentModelStubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_model_stubs', function (Blueprint $table) {
            $table->increments('id');
            
            // Rest of migration
        });
    }
    
    // ...
}
``` 

Foreign keys are defined as unsigned integer in the migrations as following:

```php
class CreateParentModelStubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_model_stubs', function (Blueprint $table) {
            // ...
            
            $table->integer('foreign_key')->unsigned();
            
            $table
                ->foreign('foreign_key')
                ->references('id')
                ->on('foreign_table')
            ;
            
            // Rest of migration
        });
    }
    
    // ...
}
``` 
